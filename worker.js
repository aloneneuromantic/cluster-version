/**
 * Module dependencies.
 */
let debug = require('debug')('lonster-js:server'),
    http = require('http'),
    server = null,
    utils = require('./utils'),
    settingObject = {};

utils('get-server-settings')
    .then((ss) => {
        runApplication(ss);
    })
    .catch((error) => {
        console.log(error);
    });

function runApplication(settings) {
    console.log(settings);
    let app = require('./app')(settings);
    app.set('port', settings.ServerPort);
    server = http.createServer(app);
    
    
    server.listen(settings.ServerPort);
    //server.on('error', onError);
    //server.on('listening', onListening);
}
/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  let port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}
/**
* Event listener for HTTP server "error" event.
*/
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  /* handle specific listen errors with friendly messages */
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}
/**
* Event listener for HTTP server "listening" event.
*/
function onListening() {
  let addr = server.address(settingObject.ServerAddress),
      bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}