/* jshint esversion:6 */
const answerBuilder = require('../helpers/json-builder'),
      findOne = require('../helpers/find-one'),
      rget = (obj) => {
          return new Promise((resolve, reject) => {
              obj.client.hgetall(obj.hash, (err, entry) => {
                  if(err) {
                      answerBuilder({
                          res: obj.res,
                          data: {
                              error: err
                          }
                      });
                  } else {
                      if(obj === null) {
                          reject({});
                      } else {
                          resolve(entry);
                      }
                  }
              });
          });
      };

module.exports = rget;
