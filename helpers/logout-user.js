/* jshint esversion: 6  */
const User = require('../models/user'),
      Token = require('../models/token'),
      helpers = require('../helpers/helpers'),
      secretWord = ('../configs/secret-word'),
      jwt = require('jsonwebtoken'),
      answerBuilder = require('../helpers/json-builder'),
      logoutUser = (obj) => {
          User.findOne({
              phone: obj.req.decoded.username
          })
              .then((user) => {
                  if(user === null) {
                      answerBuilder({
                          res: obj.res,
                          data: {
                              type: 'User',
                              exhaust: user
                          }
                      });
                  } else {
                      let token = obj.req.body.token || obj.req.query.token || obj.req.cookies.token || obj.req.headers['x-access-token'] || undefined;
                      if(token === undefined) {
                          answerBuilder({
                              res: obj.res,
                              data: {
                                  type: 'Token',
                                  state: 'incerrected-data'
                              }
                          });
                      } else {
                          Token.findOne({
                              token: token
                          })
                              .then((tkn) => {
                                  let lst = user.tokens,
                                      max = user.tokens.length,
                                      iterator = 1,
                                      arr = [];
                                  lst.forEach((item) => {
                                      if(tkn._id.toString() !== item.toString()) {
                                          arr.push(item);
                                      }

                                      if(max === iterator) {
                                          user.tokens = arr;
                                          user.save()
                                              .then(() => {
                                                  Token.remove({
                                                      token: token
                                                  })
                                                      .then(() => {
                                                          answerBuilder({
                                                              res: obj.res,
                                                              data: {
                                                                  type: 'Token',
                                                                  state: 'success'
                                                              }
                                                          });
                                                      })
                                                      .catch((error) => {
                                                          answerBuilder({
                                                              res: obj.res,
                                                              data: {
                                                                  type: 'Token',
                                                                  error: error
                                                              }
                                                          });
                                                      });
                                              })
                                              .catch((error) => {
                                                  answerBuilder({
                                                      res: obj.res,
                                                      data: {
                                                          type: 'User',
                                                          error: error
                                                      }
                                                  });
                                              });
                                      } else {
                                          iterator +=1;
                                      }
                                  });
                              })
                              .catch((error) => {
                                  answerBuilder({
                                      res: obj.res,
                                      data: {
                                          type: 'Token',
                                          error: error
                                      }
                                  });
                              });
                      }
                  }
              })
              .catch((error) => {
                  answerBuilder({
                      res: obj.res,
                      data: {
                          type: 'User',
                          error: error
                      }
                  });
              });
      };

module.exports = logoutUser;

