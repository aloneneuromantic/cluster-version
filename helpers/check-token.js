/* jshint esversion: 6 */
const jwt = require('../helpers/check-token'),
      secretWord = require('../configs/secret-word'),
      User = require('../models/user'),
      answerBuilder = require('../helpers/json-builder');

let checkToken = (obj) => {
    let token = obj.req.body.token || obj.req.query.token || obj.req.cookies.token || obj.req.headers['x-access-token'];

    if(token === undefined){
        answerBuilder({
            res: obj.res,
            data: {
                state: 'incerrected-data'
            }
        });
    } else {
        jwt.verify(token, secretWord, (error, decoded) => {
            if(error) {
                answerBuilder({
                    res: obj.res,
                    data: {
                        type: 'Decode Token',
                        error: error
                    }
                });
            } else {
                User.findOne({
                    phone: decoded.phone
                })
                    .then((user) => {
                        answerBuilder({
                            res: obj.res,
                            data: {
                                type: 'User',
                                exhaust: user
                            },
                            meta: {
                                token: token
                            }
                        });
                    })
                    .catch((error) => {
                        answerBuilder({
                            res: obj.res,
                            data: {
                                type: 'User',
                                error: error
                            }
                        });
                    });
            }
        });
    }
};

module.exports = checkToken;
