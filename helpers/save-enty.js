/* jshint esversion: 6*/
const answerBuilder = require('../helpers/json-builder'),
      saveEntry = (obj) => {
          new obj.model(obj.newObj).save()
              .then(() => {
                  answerBuilder({
                      res: obj.res,
                      data: {
                          type: obj.type,
                          state: 'success'
                      }
                  });
              })
              .catch((error) => {
                  answerBuilder({
                      res: obj.res,
                      data: {
                          error: error
                      }
                  });
              });
      };

module.exports = saveEntry;
