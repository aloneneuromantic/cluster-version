/* jshint esversion: 6 */
const User = require('../models/user'),
      authUnsafe = require('../helpers/auth-unsafe'),
      buildJSON = require('../helpers/build-json'),
      saveEntry = require('../helpers/save-enty'),
      generatePassword = require('../helpers/generate-password'),
      answerBuilder = require('../helpers/json-builder');

let onlyAuthUnsafe = (obj) => {
    if(obj.phone === undefined) {
            answerBuilder({
                res: obj.res,
                data: {
                    type: 'User',
                    state: 'incorrected-data'
                }
            });
        } else {
            User.findOne({
                    phone: obj.phone
                })
                    .then((user) => {
                        if(user === null) {
                            answerBuilder({
                                res: obj.res,
                                data: {
                                    type: 'User',
                                    exhaust: user
                                }
                            });
                        } else {
                            authUnsafe({
                                res: obj.res,
                                client: obj.client,
                                user: user,
                                ua: obj.ua
                            });
                        }
                    })
                .catch((error) => {
                    answerBuilder({
                        res: obj.res,
                        data: {
                            error: error
                        }
                    });
                });
        }
};

module.exports = onlyAuthUnsafe;
