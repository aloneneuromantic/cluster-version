/* jshint esversion:6 */
let templateJSON = {data: {}, errors: {}, meta: {}},
    templateAnswer = { status: null, answer: null },
    failed = {
        status: 'failed',
        message: 'The operation ended with an unxpected result'
    },
    success = (type) => {
        return {
            status: 'success',
            message: `Action with type ${type} carried out successfully`
        };
    },
    notSupported = {
        status: 'empty',
        message: 'This router or method is disable. Use other method/router for use this function'
    },
    resourceNotSupport = {
        status: 'block',
        message: 'This resource is not registered on the service uniform user authorization'
    },
    buildAnswer = (status, message) => {
        templateJSON.data = message;
        templateAnswer.status = status;
        templateAnswer.answer = templateJSON;
        return templateAnswer;
    },
    buildError = (error) => {
        templateJSON.data = failed;
        templateJSON.errors = error;
        templateAnswer.status = 404;
        templateAnswer.answer = templateJSON;
        return templateAnswer;
    },
    nothingFound = (data) => {
        templateJSON.errors = {
            type: data.type,
            message: `Application can not find document(s) with type ${data.type}`
        };
        if(Array.isArray(data.exhaust)) templateJSON.data = [];
        templateAnswer.status = 404;
        templateAnswer.answer = templateJSON;
        return templateAnswer;
    },
    giveDocument = (data) => {
        templateJSON.data = {
            id: data.exhaust._id,
            type: data.type,
            attributes: data.exhaust
        };
        templateAnswer.status = 200;
        templateAnswer.answer = templateJSON;
        if(data.hasOwnProperty('meta') === true) {
            templateJSON.meta = data.meta;
        }
        return templateAnswer;
    },
    giveDocuments = (data) => {
        let max = data.exhaust.length,
            objs = [],
            iterator = 1;
        data.exhaust.forEach((obj) => {
            objs.push({
                id: obj._id,
                type: data.type,
                attributes: obj
            });

            if(max !== iterator) {
                iterator += 1;
            } else {
                templateJSON.data = objs;
                templateAnswer.status = 200;
                templateAnswer.answer = templateJSON;
            }
        });
        return templateAnswer;
    },
    undefinedError = {
        status: 'unknown',
        message: 'Server return undefined error'
    },
    incorrectedData = {
        status: 'error',
        message: 'Data is incerrect'
    },
    buildJSON = (data) => {
        if(Array.isArray(data.exhaust) === true &&
           data.exhaust.length !== 0) {
            return giveDocuments(data);
        } else {
            if(data.exhaust === null ||
               data.exhaust.length === 0 ||
               Object.keys(data.exhaust) === 0) {
                return nothingFound(data);
            } else {
                return giveDocument(data);
            }
        }
    },
    answer = (obj) => {
        if(obj.hasOwnProperty('error') === true) {
            return buildError(obj.error);
        } else {
            if(obj.hasOwnProperty('state') === false) {
                return buildJSON(obj);
            } else {
                switch(obj.state) {
                case 'success':
                    return buildAnswer(200, success(obj.type || 'Undefined Type'));
                case 'created':
                    return buildAnswer(201, success(obj.type || 'Undefined Type'));
                case 'incorrected-data':
                    return buildAnswer(404, incorrectedData);
                case 'failed':
                    return buildAnswer(404, failed);
                case 'not-support':
                    return buildAnswer(405, notSupported);
                case 'resource-not-support':
                    return buildAnswer(400, resourceNotSupport);
                default:
                    return buildAnswer(500);
                }
            }
        }
    },
    responseBuilder = (obj) => {
        let currentAnswer = answer(obj.data);
        obj.res.status(currentAnswer.status).json(currentAnswer.answer);
    };

module.exports = responseBuilder;
