/* jshint esversion: 6 */
const buildJSON = require('../helpers/build-json');

let rset = (obj, name, client) => {
    return new Promise((resolve, reject) => {
        client.hmset(obj.token,
                     'id', obj._id.toString(),
                     'user', obj.user.toString(),
                     'createdAt:', obj.createdAt,
                     'endingAt', obj.endingAt || " ", (e, o) => {
                         if(e) {
                             buildJSON(e, true, name)
                                 .then((json) => {
                                     reject(json);
                                 })
                                 .catch((error) => {
                                     reject(error);
                                 });
                         } else {
                             resolve({});
                         }
                     });
    });
};

module.exports = rset;
