/* jshint esversion: 6*/
let unless_route = (path, middleware) => {
    return (req, res, next) => {
        if(path.indexOf(req.path) === -1) {
            return middleware(req, res, next);
        } else {
            return next();
        }
    };
};

module.exports = unless_route;
