/* jshint esversion:6 */
//buildJSON  = require('../helpers/build-json');
const answerBuilder = require('../helpers/json-builder'),
      findAll = (obj) => {
          obj.model.find({})
              .then((lst) => {
                  answerBuilder({
                      res: obj.res,
                      data: {
                          type: obj.type,
                          exhaust: lst
                      }
                  });
              })
              .catch((err) => {
                  answerBuilder({
                      res: obj.res,
                      data: {
                          error: err
                      }
                  });
              });
      };

module.exports = findAll;
