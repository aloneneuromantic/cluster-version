/* jshint esversion:6 */
let answerJSON = function(state, type) {
    return new Promise((resolve, reject) => {
        if(state === 'success') {
            resolve({
                "data": {
                    "status": "success",
                    "message": `Actions with ${type} carried out successfully`
                },
                "errors": {},
                "meta": {}
            });
        } else if(state === 'not-support') {
            resolve({
                "data": {
                    "status": "empty",
                    "message": "This router is disable. User POST or PUT for use this function"
                },
                "errors": {},
                "meta": {}
            });
        } else if(state === 'resource-not-support'){
            resolve({
                "data": {
                    "status": "block",
                    "message": "This router is disable. Please add resource in headers!"
                },
                "errors": {},
                "meta": {}
            });
        } else {
            reject({
                "data": {
                    "status": "failed",
                    "message": "The operation ended with an unexpected result"
                },
                "errors": {},
                "meta": {
                    "state": state,
                    "type": type
                }
            });
        }
    });
};

module.exports = answerJSON;
