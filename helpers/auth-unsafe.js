/* jshint esversion:6 */
const Token = require('../models/token'),
      buildJSON = require('../helpers/build-json'),
      answerJSON = require('../helpers/answer-json'),
      secretWord = require('../configs/secret-word'),
      jwt = require('jsonwebtoken'),
      saveToRedis = require('../helpers/save-to-redis'),
      answerBuilder = require('../helpers/json-builder');

let authUnsafe = (obj) => {
    let token = jwt.sign({
        isUnsafe: true,
        username: obj.user.phone,
        fio: obj.user.fio,
        referrer: obj.referrer,
        type: obj.user.type
    }, secretWord, {
        expiresIn: 60 * 60 * 24
    });

    new Token({
        token: token,
        user: obj.user._id,
        agent: obj.ua
    }).save()
        .then((t) => {
            saveToRedis(t, 'Token', obj.client)
                .then(() => {
                    let idToken = t._id,
                        tokens = obj.user.tokens;

                    tokens.push(idToken);
                    obj.user.tokens = tokens;
                    obj.user.save()
                        .then((u) => {
                            answerBuilder({
                                   res: obj.res,
                                   data: {
                                       type: 'User',
                                       exhaust: u,
                                       meta: {
                                           isUnsafe: true,
                                           token: t.token
                                       }
                                   }
                               });
                        })
                        .catch((e) => {
                                answerBuilder({
                                    res: obj.res,
                                    data: {
                                        error: e
                                    }
                                });
                            });
                })
                .catch((error) => {
                    answerBuilder({
                        res: obj.res,
                        data: {
                            error: error
                        }
                    });
                });
        })
        .catch((error) => {
            answerBuilder({
                res: obj.res,
                data: {
                    error: error
                }
            });
        });
};

module.exports = authUnsafe;
