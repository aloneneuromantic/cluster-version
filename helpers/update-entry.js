/* jshint esversion:6 */
const answerBuilder = require('../helpers/json-builder'),
      updateEntry = (obj) => {
          obj.model.findOne(obj.filters)
              .then((resultat) => {
                  let lst = Object.keys(obj.update),
                      max = lst.length,
                      iterator = 1;
                  lst.forEach((item) => {
                      if(item !== '_id' &&
                         item !== 'user' &&
                         item !== 'password' &&
                         item !== 'token' &&
                         item !== 'createdAt') {
                          resultat[item] = update[item];
                      }

                      if(max === iterator) {
                          resultat.save()
                              .then(() => {
                                  answerBuilder({
                                      res: obj.res,
                                      data:{
                                          type: obj.type,
                                          state: 'success'
                                      }
                                  });
                              })
                              .catch((error) => {
                                  answerBuilder({
                                      res: obj.res,
                                      data: {
                                          error: error
                                      }
                                  });
                              });
                      } else {
                          iterator += 1;
                      }
                  });
              })
              .catch((error) => {
                  answerBuilder({
                      res: obj.res,
                      data: {
                          error: error
                      }
                  });
              });
      };

module.exports = updateEntry;

