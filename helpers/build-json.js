/* jshint esversion:6 */

let buildJSON = function(data, isError, type) {
    return new Promise((resolve,reject) => {
        if(isError === true) {
            if(data) {
                resolve({
                    "data": {
                        "status": "failed",
                        "message": `Aplication error, when work woth ${type} Schema`
                    },
                    "errors": data,
                    "meta": {}
                });
            } else {
                reject({
                    "data": {
                        "status": "failed",
                        "message": "Error! Empty error data!"
                    },
                    "errors": {},
                    "meta": {}
                });
            }
        } else {
            if(Array.isArray(data) === true) {
                let max = data.length,
                    objs = [],
                    iterator = 1;
                if(max > 0) {
                    data.forEach((obj) => {
                        objs.push({
                            "id": obj._id,
                            "type": type,
                            "attributes": obj
                        });

                        if(max === iterator) {
                            resolve({
                                "data": objs,
                                "errors": {},
                                "meta": {
                                    "count": objs.length
                                }
                            });
                        } else {
                            iterator += 1;
                        }

                    });
                } else {
                    reject({
                        "data": [],
                        "errors": {
                            "type": type,
                            "message": `Empty main data with type ${type}`
                        },
                        "meta": {
                            "data": data,
                            "isError": isError,
                            "type": type
                        }
                    });
                }
            } else {
                if(data === null) {
                    reject({
                        "data": {},
                        "errors": {
                            "type": type,
                            "message": `Application can not find object with type ${type}`
                        },
                        "meta": {}
                    });
                } else {
                    if(Object.keys(data).length !== 0) {
                        resolve({
                            "data": {
                                "id": data._id,
                                "type": type,
                                "attributes": data
                            },
                            "errors": {},
                            "meta": {}
                        });
                    } else {
                        reject({
                            "data": [],
                            "errors": {},
                            "meta": {
                                "data": data,
                                "isError": isError,
                                "type": type
                            }
                        });
                    }
                }
            }
        }
    });
};

module.exports = buildJSON;
