/* jshint esversion: 6*/
const User = require('../models/user'),
      Token = require('../models/token'),
      validatePassword = require('../helpers/validate-password'),
      secretWord = require('../configs/secret-word'),
      jwt = require('jsonwebtoken'),
      saveToRedis = require('../helpers/save-to-redis'),
      answerBuilder = require('../helpers/json-builder'),
      authUser = (obj) => {
          let token = obj.req.body.token || obj.req.query.token || obj.req.cookies.token || obj.req.headers['x-access-token'];
          if(token === undefined) {
              if(obj.req.body.password === undefined || obj.req.body.phone === undefined) {
              	answerBuilder({
              		res: obj.res,
              		data: {
              			type: 'User',
              			state: 'incorrected-data'
              		}
              	});
              } else {
              	User.findOne({
                  phone: obj.req.body.phone
              	})
                  .then((user) => {
                      if(user === null) {
                          answerBuilder({
                              res: obj.res,
                              data: {
                                  type: 'User',
                                  exhaust: user
                              }
                          });
                      } else {
                          validatePassword(obj.req.body.password, user.password)
                              .then((valid) => {
                                  if(valid !== true){
                                      answerBuilder({
                                          res: obj.res,
                                          data: {
                                              state: 'failed'
                                          }
                                      });
                                  } else {
                                      let token = jwt.sign({
                                          isUnsafe: false,
                                          username: user.phone,
                                          fio: user.fio,
                                          referrer: obj.req.body.referrer ||
                                              obj.req.query.referrer ||
                                              obj.req.cookies.referrer ||
                                              obj.req.referrer ||
                                              obj.req.headers.referrer || undefined,
                                          type: user.type
                                      }, secretWord, {
                                          expiresIn: 60 * 60 * 24
                                      });
                                      new Token({
                                          token: token,
                                          user: user._id,
                                          agent: obj.req.body.userAgent || 'empty'
                                      }).save()
                                          .then((resultat) => {
                                              saveToRedis(resultat, 'Token', obj.client)
                                                  .then((t) => {
                                                      let idToken = resultat._id,
                                                          tokens = user.tokens;
                                                      tokens.push(idToken);
                                                      user.tokens = tokens;
                                                      user.save()
                                                          .then(() => {
                                                              answerBuilder({
                                                                  res: obj.res,
                                                                  data: {
                                                                      type: 'User',
                                                                      exhaust: user,
                                                                      meta: {
                                                                          token: token
                                                                      }
                                                                  }
                                                              });
                                                          })
                                                          .catch((error) => {
                                                              answerBuilder({
                                                                  res: obj.res,
                                                                  data: {
                                                                      type: 'User',
                                                                      error: error
                                                                  }
                                                              });
                                                          });
                                                  })
                                                  .catch((error) => {
                                                      answerBuilder({
                                                          res: obj.res,
                                                          data: {
                                                              type: 'Redis',
                                                              error: error
                                                          }
                                                      });
                                                  });
                                          })
                                          .catch((error) => {
                                              answerBuilder({
                                                  res: obj.res,
                                                  data: {
                                                      type: 'Token',
                                                      error: error
                                                  }
                                              });
                                          });
                                  }
                              })
                              .catch((error) => {
                                  answerBuilder({
                                      req: obj.res,
                                      data: {
                                          type: 'Validation Password',
                                          error: {
                                              error: error
                                          }
                                      }
                                  });
                              });
                      }
                  })
                  .catch((error) => {
                      answerBuilder({
                          res: obj.res,
                          data: {
                              type: 'User',
                              error: error
                          }
                      });
                  });
              }
          } else {
              jwt.verify(token, secretWord, (error, decoded) => {
                  if(error) {
                      answerBuilder({
                          res: obj.res,
                          data: {
                              type: 'Token',
                              error: error
                          }
                      });
                  } else {
                      User.findOne({
                          phone: decoded.phone
                      })
                          .then((user) => {
                              if(user === null) {
                                  answerBuilder({
                                      res: obj.res,
                                      data: {
                                          type: 'User',
                                          exhaust: user
                                      }
                                  });
                              } else {
                                  answerBuilder({
                                      res: obj.res,
                                      data: {
                                          type: 'User',
                                          exhaust: user
                                      },
                                      meta: {
                                          token: token
                                      }
                                  });
                              }
                          })
                          .catch((error) => {
                              answerBuilder({
                                  res: obj.res,
                                  data: {
                                      error: error
                                  }
                              });
                          });
                  }
              });
          }
};


module.exports = authUser;
