/* jshint esversion: 6 */
// Helpers
const answerBuilder = require('../helpers/json-builder'),
      findThis = (obj) => {
          obj.model.findOne(obj.filters)
              .then((resultat) => {
                  answerBuilder({
                      res: obj.res,
                      data: {
                          type: obj.type,
                          exhaust: resultat
                      }
                  });
              })
              .catch((error) => {
                  answerBuilder({
                      res: obj.res,
                      data: {
                          error: error
                      }
                  });
              });
      };

module.exports = findThis;

