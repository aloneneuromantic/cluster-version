/* jshint esversion:6 */

const pbkdf2 = require('pbkdf2-sha256'),
      validatePassword = (key, string) => {
          return new Promise((resolve) => {
              let parts = string.split('$'),
                  iterations = parts[1],
                  salt = parts[2],
                  resultat = null;
              if(iterations === undefined || salt === undefined) {
                  iterations = 20000;
                  resultat = (pbkdf2(key, new Buffer('O52lrWngfxVv'), 20000, 32).toString('base64') === string);
                  if(resultat === true) {
                      resolve(true);
                  } else {
                      resolve(false);
                  }
              } else {
                  resultat = (pbkdf2(key, new Buffer(salt), iterations, 32).toString('base64') === parts[3]);
                  if(resultat === true) {
                      resolve(true);
                  } else {
                      resolve(false);
                  }
              }
          });
      };

module.exports = validatePassword;
