/* jshint esversion: 6 */
const Role = require('../models/role'),
      UserType = require('../models/user-type'),
      User = require('../models/user'),
      ResourceType = require('../models/resource-type');

let  validateRoleAndTypePrivate = (resource, decodedToken, method) => {
    return new Promise((resolve, reject) => {
        Promise.all([
            ResourceType.findOne({
                _id: resource.type.toString()
            }),

            UserType.findOne({
                _id: decodedToken.type
            })

        ])
            .then((values) => {
                if(values[1].name !== values[0].name) {
                    reject({
                        "status": "failed",
                        "message": "User type does not match the resource"
                    });
                } else {
                    Role.findOne({
                        _id: values[1].role
                    })
                        .then((role) => {
                            if(method === 'POST' && role.create === false) {
                                reject({
                                    "status": "failed",
                                    "message": "Do not have permission to perform the action"
                                });
                            } else if(method === 'PUT' && role.edit === false) {
                                reject({
                                    "status": "failed",
                                    "message": "Do not have permission to perform the action"
                                });
                            } else if(method === 'DELETE' && role.delete === false) {
                                reject({
                                    "status": "failed",
                                    "message": "Do not have permission to perform the action"
                                });
                            } else {
                                resolve({
                                    "status": "success",
                                    "message": "The request may be made"
                                });
                            }
                        })
                        .catch((error) => {
                            reject({
                                "status": "failed",
                                "message": "Can not find role"
                            });
                        });
                }
            })
            .catch((error) => {
                reject(error);
            });
    });
};

module.exports = validateRoleAndTypePrivate;
