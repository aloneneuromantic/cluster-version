/* jshint esversion: 6 */
const pbkdf2 = require('pbkdf2-sha256');

let generatePassword = (key) => {
    return pbkdf2(key, new Buffer('O52lrWngfxVv'), 20000, 32).toString('base64');
};

module.exports = generatePassword;
