/* jshint esversion: 6 */
const authUser = require('../helpers/auth-user'),
      authUnsafe = require('../helpers/auth-unsafe'),
      generatePassword = require('../helpers/generate-password'),
      saveEntry = require('../helpers/save-enty'),
      User = require('../models/user'),
      answerBuilder = require('../helpers/json-builder'),
      regOrAuth = (obj) => {
          let token = obj.req.body.token || obj.req.query.token || obj.req.cookies.token || obj.req.headers['x-access-token'] || undefined;
          if(token === undefined) {
              answerBuilder({
                  res: obj.res,
                  data: {
                      type: 'Token',
                      state: 'incerrected-data'
                  }
              });
          } else {
              if(!obj.req.body.phone) {
                  answerBuilder({
                      res: obj.res,
                      data: {
                          type: 'Token',
                          state: 'incerrected-date'
                      }
                  });
              } else {
                  User.findOne({
                      phone: obj.req.body.phone
                  })
                      .then((user) => {
                          if(user === null) {
                              const reqObj = obj.req.body,
                                    hash = generatePassword(reqObj.password);
                              reqObj.password = hash;
                              saveEntry({
                                  res: obj.res,
                                  model: User,
                                  type: user,
                                  newObj: reqObj
                              });
                          } else {
                              authUnsafe({
                                  res: obj.res,
                                  client: obj.client,
                                  user: user,
                                  ua: obj.req.body.userAgent || 'empty'
                              });
                          }
                      })
                      .catch((error) => {
                          answerBuilder({
                              res: obj.res,
                              data: {
                                  type: 'User',
                                  error: error
                              }
                          });
                      });
              }
          }
      };

module.exports = regOrAuth;
