const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      Holding = new Schema({
          name: {
              type: String,
              uniquie: true
          },
          head: {
              type: Schema.Types.ObjectId,
              ref: 'Company'
          },
          tail: [{
              type: Schema.Types.ObjectId,
              ref: 'Company'
          }],
          comments: String
      });

module.exports = mongoose.model('Holding', Holding);
