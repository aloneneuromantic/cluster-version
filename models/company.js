/* jshint esversion: 6 */
const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      Company = new Schema({
          name: {
              type: String,
              unique: true
          },
          group: {
            type: Schema.Types.ObjectId,
              ref: 'Holding'
          },
          resources: [{
              type: Schema.Types.ObjectId,
              ref: 'Resource'
          }],
          users: [{
              type: Schema.Types.ObjectId,
              ref: 'User'
          }],
          tokens: [{
              type: Schema.Types.ObjectId,
              ref: 'Token'
          }],
          data: {
              kpp: String,
              ogrn: String,
              fullname: String,
              phone: String,
              email: String,
              address: String,
              comments: String
          },
          registered: {
              type: Date,
              default: Date.now
          }
      });

module.exports = mongoose.model('Company', Company);
