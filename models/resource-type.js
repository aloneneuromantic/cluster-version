/* jshint esversion:6 */
const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      ResourceType  = new Schema({
          name: {
              type: String,
              unique: true
          },
          label: String
    });

module.exports = mongoose.model('ResourceType', ResourceType);
