const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let SettingEntry = new Schema({
        name: String,
        value: String
    });

module.exports = mongoose.model('SettingEntry', SettingEntry);