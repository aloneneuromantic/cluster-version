/* jshint esversion:6 */
const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      Resource = new Schema({
          type: {
             type: Schema.Types.ObjectId,
             ref: 'ResourceType'
            },
          name: {
              type: String,
              unique: true
          },
          label: String,
          url: String
    });

module.exports = mongoose.model('Resource', Resource);
