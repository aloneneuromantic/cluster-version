/*jshint esversion:6*/
const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      User = new Schema({
          // Logic variables
          full_fio: {
              type: Boolean,
              default: false
          },
          sale_used: {
              type: Boolean,
              default: false
          },
          is_verified_phone: {
              type: Boolean,
              default: false
          },
          is_verified_emai: {
              type: Boolean,
              default: false
          },
          can_see_logic_reports: {
              type: Boolean,
              default: false
          },
          is_registration_complete: {
              type: Boolean,
              default: false
          },
          // Password (crypt)
          password: String,
          // Real attributes
          price_type: {
              type: String,
              default: 'price',
              enum: [
                  'price',
                  'dcprice1', 'dcprice2', 'dcprice3',
                  'optprices001', 'optprices002', 'optprices003'
              ]
          },
          fio: String,
          city_id: Number,
          customer_type: {
              type: String,
              deafault: 'b2c',
              enum: ['b2b', 'b2c']
          },
          phone: {
              type: String,
              unique: true
          },
          managers: [{
              type: Schema.Types.ObjectId,
              res: 'User'
          }],
          updated_at: {
              type: Date,
              default: Date.now
          },
          // New Fields:
          contacts: [{
              name: String,
              value: String
          }],
          type: {
              type: Schema.Types.ObjectId,
              ref: 'UserType'
          },
          tokens: [{
              type: Schema.Types.ObjectId,
              ref: 'Token'
          }],
          company: {
              type: Schema.Types.ObjectId,
              ref: 'Company'
          }
      });

module.exports = mongoose.model('User', User);
