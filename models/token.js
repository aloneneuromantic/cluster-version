/* jshint esversion: 6 */
const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      Token = new Schema({
          token: {
              type: String,
              unique: true
          },
          user: {
              type: Schema.Types.ObjectId,
              ref: 'User'
          },
          agent: String,
          createdAt: {
              type: Date,
              default: Date.now
          },
          endingAt: Date
      });

module.exports = mongoose.model('Token', Token);
