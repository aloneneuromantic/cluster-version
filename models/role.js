/* jshint esversion:6 */
const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      Role = new Schema({
          name: {
              type: String,
              unique: true
          },
          label: String,
          resource: {
              type: Schema.Types.ObjectId,
              ref: 'Resource'
          },
          create: {
              type: Boolean,
              default: false
          },
          delete: {
              type: Boolean,
              default: false
          },
          edit: {
              type: Boolean,
              default: false
          },
          read: {
              type: Boolean,
              default: true
          }
      });

module.exports = mongoose.model('Role', Role);
