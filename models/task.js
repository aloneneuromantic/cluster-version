const mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
let Task = new Schema({
        event: String,
        action: String
    });

module.exports = Task;