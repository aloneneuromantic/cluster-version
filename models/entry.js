/* jshint esversion: 6 */
const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      Entry = new Schema({
          title: String,
          status: String,
          message: String,
          date: {
              type: Date,
              default: Date.now
          }
      });

module.exports = mongoose.model('Entry', Entry);
