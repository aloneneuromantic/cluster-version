/* jshint esversion:6 */
const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      UserType = new Schema({
          name: {
              type: String,
              unique: true
          },
          label: String,
          role: {
              type: Schema.Types.ObjectId,
              ref: 'Role'
          }
      });

module.exports = mongoose.model('UserType', UserType);
