/*jshint esversion: 6*/
const mongoose = require('mongoose'),
    mongoSettings = require('./configs/mongo-settings'),
    mongoExpressConfig = require('./configs/mongo-express'),
    SettingEntry = require('./models/setting-entry');
let cs = null;


mongoose.Promise = Promise;


if(mongoSettings.password === 'none') {
    cs = `mongodb://${mongoSettings.host}:${mongoSettings.port}/db`;
} else {
    cs = `mongodb://${mongoSettings.login}:${mongoSettings.password}@${mongoSettings.host}:${mongoSettings.port}/db`;
}


function returnConnectionString() {
    return new Promise((resolve,reject) => {
        getMongoSettings()
            .then((ms) => {
                    let lcs = null;
                    if(ms.MongoPassword === 'none') {
                        lcs = `mongodb://${ms.MongoHost}:${ms.MongoPort}/db`;
                        resolve(lcs);
                    } else {
                        lcs = `mongodb://${ms.adminLogin}:${ms.adminPassword}@${ms.MongoHost}:${ms.MongoPort}/db`;
                        resolve(lcs);
                    }
                })
            .catch((error) => {
                    console.log(error);
                    reject(cs);
                });
    });
}


function getServerSettings() {
    return new Promise((resolve, reject) => {
            console.log('Open connection');
            mongoose.connect(cs);
            SettingEntry.find({})
            .then((entries) => {
                    let obj = {},
                        max = entries.length,
                        iterator = 1;
                    entries.forEach((entry) => {
                            if(entry.name === 'ServerPort' ||
                               entry.name === 'ServerAddress') {
                                obj[entry.name] = entry.value;
                            }
                            iterator += 1;
                            if(iterator === max) {
                                mongoose.connection.close()
                                .then(() => {
                                        console.log('Close connection');
                                        resolve(obj);
                                    })
                                .catch((err) => {
                                        console.log(err);
                                    });
                            }
                        });
                })
            .catch((error) => {
                    reject(error);
                });
        });
}


function getMongoSettings() {
    return new Promise((resolve, reject) => {
            mongoose.connect(cs);
            SettingEntry.find({})
            .then((entries) => {
                    let obj = {},
                        max = entries.length,
                        iterator = 1;

                    entries.forEach((entry) => {
                            if(entry.name === 'MongoHost' ||
                               entry.name === 'MongoPort' ||
                               entry.name === 'AdminLogin' ||
                               entry.name === 'MongoPassword') {
                               obj[entry.name] = entry.value; 
                            }
                            iterator += 1;
                            if(iterator === max) {
                                mongoose.connection.close()
                                .then(() => {
                                        resolve(obj);
                                    })
                                .catch((error) => {
                                        reject(error);
                                    });
                            }
                        });
                })
            .catch((error) => {
                reject(error);
            });
        });
}


function getAllSettings() {
    return new Promise((resolve, reject) => {
            mongoose.connect(cs);
            SettingEntry.find({})
            .then((entries) => {
                    mongoose.connection.close();
                    resolve(entries);
                })
            .catch((error) => {
                reject(error);
            });
        });
}


function getMongoExpressSettings() {
    return new Promise((resolve, reject) => {
           mongoose.connect(cs);
           SettingEntry.find({})
            .then((entries) => {
                mongoose.connection.close()
                    .then(() => {
                        console.log(entries);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                reject(error);
            });
        });
}

let utils = function (wtf) {
    switch(wtf) {
    case 'get-mongo-settings':
        return getMongoSettings();
    case 'get-mongo-express-settings':
        return getMongoExpressSettings();
    case 'get-all-settings':
        return getAllSettings();
    case 'get-server-settings':
        return getServerSettings();
    case 'get-connection-string':
        return returnConnectionString();
    default:
        return getAllSettings();
    }
};


module.exports = utils;
