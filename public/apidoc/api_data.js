define({ "api": [
  {
    "type": "post",
    "url": "/lor",
    "title": "Request to authorization or register user",
    "name": "Authorize",
    "group": "Development",
    "version": "0.0.1",
    "permission": [
      {
        "name": "For all  users"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT user token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Phone user (user login)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of user (not hashed)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>An array of tokens</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": "<p>An empty object with errors</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>An object with additional data (counter)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response (login):",
          "content": "HTTP 200 OK\n{\n    \"data\": {\n           \"id\": \"5864bdfb9885ad5e9b73aff8\",\n           \"type\": \"User\",\n          \t\"attributes\": {\n                 \"_id\": \"5864bdfb9885ad5e9b73aff8\",\n                 \"phone\": \"89969464629\",\n                 \"password\": \"gxvWyD/1MNO0MS4Av2QjeVTBlBTn8Ouutiw1/RBSKpw=\",\n                 \"fio\": \"Иванов Иван Иванович\",\n                 \"type\": \"587cadf1618c40564d447839\",\n                 \"__v\": 71,\n                 \"tokens\": [\n                     \"5875b6619d0906295bc96e40\"\n                 ],\n                 \"contacts\": [],\n                 \"updated_at\": \"2016-12-29T07:40:43.586Z\",\n                 \"managers\": [],\n                 \"price_type\": \"price\",\n                 \"is_registration_complete\": false,\n                 \"can_see_logic_reports\": false,\n                 \"is_verified_emai\": false,\n                 \"is_verified_phone\": false,\n                 \"sale_used\": false,\n                 \"full_fio\": false\n        }\n     },\n     \"errors\": {},\n     \"meta\": {}\n}",
          "type": "json"
        },
        {
          "title": "Success-Response (register):",
          "content": "HTTP 200 OK\n{\n    \"data\": {\n        \"status\": \"success\",\n        \"message\": \"Actions with User carried out successfully\"\n     },\n     \"errors\": {},\n     \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "referrer",
            "description": "<p>Login resource authorization system</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "IncorrectData",
            "description": "<p>404 Incorrect data, user not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "IncorrectData:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api.js",
    "groupTitle": "Development"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Request to authorization",
    "name": "Authorize",
    "group": "Development",
    "version": "0.0.1",
    "permission": [
      {
        "name": "For all  users"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT user token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Phone user (user login)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of user (not hashed)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>An array of tokens</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": "<p>An empty object with errors</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>An object with additional data (counter)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n    \"data\": {\n           \"id\": \"5864bdfb9885ad5e9b73aff8\",\n           \"type\": \"User\",\n          \t\"attributes\": {\n                 \"_id\": \"5864bdfb9885ad5e9b73aff8\",\n                 \"phone\": \"89969464629\",\n                 \"password\": \"gxvWyD/1MNO0MS4Av2QjeVTBlBTn8Ouutiw1/RBSKpw=\",\n                 \"fio\": \"Иванов Иван Иванович\",\n                 \"type\": \"587cadf1618c40564d447839\",\n                 \"__v\": 71,\n                 \"tokens\": [\n                     \"5875b6619d0906295bc96e40\"\n                 ],\n                 \"contacts\": [],\n                 \"updated_at\": \"2016-12-29T07:40:43.586Z\",\n                 \"managers\": [],\n                 \"price_type\": \"price\",\n                 \"is_registration_complete\": false,\n                 \"can_see_logic_reports\": false,\n                 \"is_verified_emai\": false,\n                 \"is_verified_phone\": false,\n                 \"sale_used\": false,\n                 \"full_fio\": false\n        }\n     },\n     \"errors\": {},\n     \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "referrer",
            "description": "<p>Login resource authorization system</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "IncorrectData",
            "description": "<p>404 Incorrect data, user not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "IncorrectData:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api.js",
    "groupTitle": "Development"
  },
  {
    "type": "post",
    "url": "/tokens/:token",
    "title": "Check token to authorization",
    "name": "CheckToken",
    "group": "Development",
    "version": "0.0.1",
    "permission": [
      {
        "name": "Authorized users only"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT user token</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "referrer",
            "description": "<p>Login resource authorization system</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>An array of token attributes</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": "<p>An empty object with errors</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>An object with additional data (counter)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n   \"data\": {\n        \"type\": \"Token\",\n        \"attributes\": {\n        \"id\": \"587e0cc1277cd23513ab211b\",\n        \"user\": \"587ddc5fa558914e64e9c026\",\n        \"createdAt:\": \"Tue Jan 17 2017 17:23:29 GMT+0500 (+05)\",\n        \"endingAt\": \" \"\n      }\n   },\n   \"errors\": {},\n   \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokentNotTransfered",
            "description": "<p>404 Not transferred token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ReferrerNotTransfered",
            "description": "<p>404 Not transfered resource token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "TokentNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        },
        {
          "title": "ReferrerNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api.js",
    "groupTitle": "Development"
  },
  {
    "type": "post",
    "url": "/users",
    "title": "Create new User",
    "name": "CreateUser",
    "group": "Development",
    "version": "0.0.1",
    "permission": [
      {
        "name": "Authorized users only"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>User login (shoul be a string, usually the user's phone )</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "referrer",
            "description": "<p>Login resource authorization system</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>An object with status operation &amp; message with description</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": "<p>An empty object with errors</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>An empry object</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n    \"data\": {\n        \"status\": \"success\",\n        \"message\": \"Actions with User carried out successfully\"\n     },\n     \"errors\": {},\n     \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokentNotTransfered",
            "description": "<p>404 Not transferred token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ReferrerNotTransfered",
            "description": "<p>404 Not transfered resource token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "AttributesNotSend",
            "description": "<p>501 Not transfered some attributes</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "AttributesNotSend:",
          "content": "HTTP 500 Internal server error\n{\n  \"name\": \"AssertionError\",\n  \"actual\": false,\n  \"expected\": true,\n  \"operator\": \"==\",\n  \"message\": \"key must be a string or buffer\",\n  \"generatedMessage\": false\n}",
          "type": "json"
        },
        {
          "title": "TokentNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        },
        {
          "title": "ReferrerNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api.js",
    "groupTitle": "Development"
  },
  {
    "type": "put",
    "url": "/users/:phone",
    "title": "Save edit user information",
    "name": "EditUser",
    "group": "Development",
    "version": "0.0.1",
    "permission": [
      {
        "name": "Authorized users only"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>User login (shoul be a string, usually the user's phone)</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "json",
            "description": "<p>JSON with user attributes</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "referrer",
            "description": "<p>Login resource authorization system</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>An object with status operation &amp; message with description</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": "<p>An empty object with errors</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>An empry object</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n   \"data\": {\n      \"status\": \"success\",\n      \"message\": \"Actions with User carried out successfully\"\n   },\n   \"errors\": {},\n   \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokentNotTransfered",
            "description": "<p>404 Not transferred token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ReferrerNotTransfered",
            "description": "<p>404 Not transfered resource token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "LoginError",
            "description": "<p>409 Phone/login not find</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "AttributesNotSend:",
          "content": "HTTP 409 Conflict\n{\n   \"data\": {},\n   \"errors\": {\n      \"type\": \"User\",\n      \"message\": \"Application can not find object with type User\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        },
        {
          "title": "TokentNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        },
        {
          "title": "ReferrerNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api.js",
    "groupTitle": "Development"
  },
  {
    "type": "get",
    "url": "/tokens/:token",
    "title": "Get all atribute one token",
    "name": "GetTokenInfo",
    "group": "Development",
    "version": "0.0.1",
    "permission": [
      {
        "name": "Authorized users only"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT user token</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "referrer",
            "description": "<p>Login resource authorization system</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>An array of token attributes</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": "<p>An empty object with errors</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>An object with additional data (counter)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n  {\n    \"data\": {\n       \"type\": \"Token\",\n       \"attributes\": {\n         \"id\": \"587d99b2b0c5d976bffe77b9\",\n         \"user\": \"5864bdfb9885ad5e9b73aff8\",\n         \"createdAt:\": \"Tue Jan 17 2017 09:12:34 GMT+0500 (+05)\",\n         \"endingAt\": \" \"\n       }\n    },\n    \"errors\": {},\n    \"meta\": {}\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokentNotTransfered",
            "description": "<p>404 Not transferred token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ReferrerNotTransfered",
            "description": "<p>404 Not transfered resource token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "TokentNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        },
        {
          "title": "ReferrerNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api.js",
    "groupTitle": "Development"
  },
  {
    "type": "get",
    "url": "/tokens",
    "title": "Request list of tokens",
    "name": "GetTokens",
    "group": "Development",
    "version": "0.0.1",
    "permission": [
      {
        "name": "Authorized users only"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT user token</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "referrer",
            "description": "<p>Login resource authorization system</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>An array of tokens</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": "<p>An empty object with errors</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>An object with additional data (counter)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n  \"data\": [\n           {\n               \"id\": \"58789b22dc915610bffa8f3e\",\n               \"type\": \"Token\",\n               \"attributes\": {\n               \"_id\": \"58789b22dc915610bffa8f3e\",\n               \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc1Vuc2FmZSI6ZmFsc2UsInVzZXJuYW1lIjoiODk5Njk0NjQ2MjkiLCJmaW8iOiLQmNCy0LDQvdC-0LIg0JjQstCw0L0g0JjQstCw0L3QvtCy0LjRhyIsImlhdCI6MTQ4NDI5OTA0MiwiZXhwIjoxNDg0Mzg1NDQyfQ.yqFait4TFRuJsL1S6ZhoVomvnR5lBT13iS1Jax5TEnA\",\n               \"user\": \"5864bdfb9885ad5e9b73aff8\",\n               \"__v\": 0,\n               \"createdAt\": \"2017-01-13T09:17:22.195Z\"\n           }\n         ],\n   \"errors\": {},\n   \"meta\": {\n            \"count\": 1\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokentNotTransfered",
            "description": "<p>404 Not transferred token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ReferrerNotTransfered",
            "description": "<p>404 Not transfered resource token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "TokentNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        },
        {
          "title": "ReferrerNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api.js",
    "groupTitle": "Development"
  },
  {
    "type": "get",
    "url": "/users/:phone",
    "title": "Get user information",
    "name": "GetUser",
    "group": "Development",
    "version": "0.0.1",
    "permission": [
      {
        "name": "Authorized users only"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>User login (shoul be a string, usually the user's phone)</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "referrer",
            "description": "<p>Login resource authorization system</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>An object with status operation &amp; message with description</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": "<p>An empty object with errors</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>An empry object</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n    \"data\": {\n           \"id\": \"5864bdfb9885ad5e9b73aff8\",\n           \"type\": \"User\",\n          \t\"attributes\": {\n                 \"_id\": \"5864bdfb9885ad5e9b73aff8\",\n                 \"phone\": \"89969464629\",\n                 \"password\": \"gxvWyD/1MNO0MS4Av2QjeVTBlBTn8Ouutiw1/RBSKpw=\",\n                 \"fio\": \"Иванов Иван Иванович\",\n                 \"type\": \"587cadf1618c40564d447839\",\n                 \"__v\": 71,\n                 \"tokens\": [\n                     \"5875b6619d0906295bc96e40\"\n                 ],\n                 \"contacts\": [],\n                 \"updated_at\": \"2016-12-29T07:40:43.586Z\",\n                 \"managers\": [],\n                 \"price_type\": \"price\",\n                 \"is_registration_complete\": false,\n                 \"can_see_logic_reports\": false,\n                 \"is_verified_emai\": false,\n                 \"is_verified_phone\": false,\n                 \"sale_used\": false,\n                 \"full_fio\": false\n        }\n     },\n     \"errors\": {},\n     \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokentNotTransfered",
            "description": "<p>404 Not transferred token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ReferrerNotTransfered",
            "description": "<p>404 Not transfered resource token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "LoginError",
            "description": "<p>409 Phone/login not find</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "AttributesNotSend:",
          "content": "HTTP 409 Conflict\n{\n   \"data\": {},\n   \"errors\": {\n      \"type\": \"User\",\n      \"message\": \"Application can not find object with type User\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        },
        {
          "title": "TokentNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        },
        {
          "title": "ReferrerNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api.js",
    "groupTitle": "Development"
  },
  {
    "type": "get",
    "url": "/users",
    "title": "Request list of Users",
    "name": "GetUsers",
    "group": "Development",
    "version": "0.0.1",
    "permission": [
      {
        "name": "Authorized users only"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT user token</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "referrer",
            "description": "<p>Login resource authorization system</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>An array of tokens</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": "<p>An empty object with errors</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>An object with additional data (counter)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n  \"data\": [\n            {\n              \"id\": \"5862211f9308113f11e0d0fe\",\n              \"type\": \"User\",\n              \"attributes\": {\n                              \"_id\": \"5862211f9308113f11e0d0fe\",\n                              \"password\": \"123123213\",\n                              \"fio\": \"Троллкина Лолка Васильевна\",\n                              \"city_id\": 1,\n                              \"customer_type\": \"b2c\",\n                              \"phone\": \"89969464620\",\n                              \"type\": \"5860e012c1bdd31318a909ae\",\n                              \"company\": \"5860d7efde603779d12580a6\",\n                              \"__v\": 0,\n                              \"tokens\": null,\n                              \"contacts\": null,\n                              \"updated_at\": \"2016-12-27T08:06:55.141Z\",\n                              \"managers\": null,\n                              \"price_type\": \"price\",\n                              \"is_registration_complete\": false,\n                              \"can_see_logic_reports\": false,\n                              \"is_verified_emai\": false,\n                              \"is_verified_phone\": false,\n                              \"sale_used\": false,\n                              \"full_fio\": true\n                            }\n             }\n         ],\n   \"errors\": {},\n   \"meta\": {\n            \"count\": 1\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokentNotTransfered",
            "description": "<p>404 Not transferred token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ReferrerNotTransfered",
            "description": "<p>404 Not transfered resource token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "TokentNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        },
        {
          "title": "ReferrerNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api.js",
    "groupTitle": "Development"
  },
  {
    "type": "post",
    "url": "/logout",
    "title": "Logout user token",
    "name": "Logout",
    "group": "Development",
    "version": "0.0.1",
    "permission": [
      {
        "name": "Authorized users only"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT user token</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "referrer",
            "description": "<p>Login resource authorization system</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>An array of tokens</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": "<p>An empty object with errors</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>An object with additional data (counter)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n    \"data\": {\n        \"status\": \"success\",\n        \"message\": \"Actions with User carried out successfully\"\n     },\n     \"errors\": {},\n     \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokentNotTransfered",
            "description": "<p>404 Not transferred token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ReferrerNotTransfered",
            "description": "<p>404 Not transfered resource token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "TokentNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        },
        {
          "title": "ReferrerNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api.js",
    "groupTitle": "Development"
  },
  {
    "type": "post",
    "url": "/register",
    "title": "Register new User (recomended)",
    "name": "RegisterUser",
    "group": "Development",
    "version": "0.0.1",
    "permission": [
      {
        "name": "For all users"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>User login (shoul be a string, usually the user's phone)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password (not hashed)</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "string",
            "optional": false,
            "field": "referrer",
            "description": "<p>Login resource authorization system</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>An object with status operation &amp; message with description</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": "<p>An empty object with errors</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>An empry object</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n    \"data\": {\n        \"status\": \"success\",\n        \"message\": \"Actions with User carried out successfully\"\n     },\n     \"errors\": {},\n     \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TokentNotTransfered",
            "description": "<p>404 Not transferred token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ReferrerNotTransfered",
            "description": "<p>404 Not transfered resource token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "AttributesNotSend",
            "description": "<p>501 Not transfered some attributes</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "AttributesNotSend:",
          "content": "HTTP 500 Internal server error\n{\n  \"name\": \"AssertionError\",\n  \"actual\": false,\n  \"expected\": true,\n  \"operator\": \"==\",\n  \"message\": \"key must be a string or buffer\",\n  \"generatedMessage\": false\n}",
          "type": "json"
        },
        {
          "title": "TokentNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        },
        {
          "title": "ReferrerNotTransfered:",
          "content": "HTTP 404 Not found\n{\n   \"data\": {},\n   \"errors\": {\n     \"type\": \"Resource\",\n     \"message\": \"Application can not find object with type Resource\"\n   },\n   \"meta\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api.js",
    "groupTitle": "Development"
  },
  {
    "type": "post",
    "url": "/ulogin",
    "title": "Request to authorization without password (unsafe method)",
    "name": "UnsafeAuthorize",
    "group": "Development",
    "version": "0.0.1",
    "permission": [
      {
        "name": "For all users"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Phone user (user login)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>An array of tokens</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": "<p>An empty object with errors</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "meta",
            "description": "<p>An object with additional data (counter)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP 200 OK\n{\n   \"data\": {\n       \"id\": \"5864bdfb9885ad5e9b73aff8\",\n       \"type\": \"User\",\n       \"attributes\": {\n             \"_id\": \"5864bdfb9885ad5e9b73aff8\",\n             \"phone\": \"89969464629\",\n             \"password\": \"gxvWyD/1MNO0MS4Av2QjeVTBlBTn8Ouutiw1/RBSKpw=\",\n             \"fio\": \"Иванов Иван Иванович\",\n             \"type\": \"587cadf1618c40564d447839\",\n             \"__v\": 90,\n             \"tokens\": [\n                    \"5875b6619d0906295bc96e40\",\n             ],\n             \"contacts\": [],\n             \"updated_at\": \"2016-12-29T07:40:43.586Z\",\n             \"managers\": [],\n             \"price_type\": \"price\",\n             \"is_registration_complete\": false,\n             \"can_see_logic_reports\": false,\n             \"is_verified_emai\": false,\n             \"is_verified_phone\": false,\n             \"sale_used\": false,\n             \"full_fio\": false\n       }\n    },\n    \"errors\": {},\n    \"meta\": {\n      \"isUnsafe\": true,\n      \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc1Vuc2FmZSI6dHJ1ZSwidXNlcm5hbWUiOiI4OTk2OTQ2NDYyOSIsImZpbyI6ItCY0LLQsNC90L7QsiDQmNCy0LDQvSDQmNCy0LDQvdC-0LLQuNGHIiwicmVmZXJyZXIiOiJ0ZXN0LWxvZ2luIiwidHlwZSI6IjU4N2NhZGYxNjE4YzQwNTY0ZDQ0NzgzOSIsImlhdCI6MTQ4NDczMjY4MywiZXhwIjoxNDg0ODE5MDgzfQ.k7K3LLjuN_gUp26ZB6OTla4gwNKuVbgyY3Em2beRGfk\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "referrer",
            "description": "<p>Login resource authorization system</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "IncorrectData",
            "description": "<p>501 Incorrect data, user not authorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "IncorrectData:",
          "content": "HTTP 501 Not implemented\n{\n    \"data\": [],\n    \"errors\": {\n        \"type\": \"User\",\n        \"message\": \"Empty main data with type User\"\n    },\n    \"meta\": {\n        \"data\": [],\n        \"isError\": false,\n        \"type\": \"User\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api.js",
    "groupTitle": "Development"
  }
] });
