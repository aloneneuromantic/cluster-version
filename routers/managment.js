/* jshint esversion: 6 */
const release = require('express').Router(),
      testing = require('express').Router(),
      development = require('express').Router(),
      // Helpers
      findAll = require('../helpers/find-all'),
      findOne = require('../helpers/find-one'),
      saveEntry = require('../helpers/save-enty'),
      updateEntry = require('../helpers/update-entry'),
      //MODELS
      ResourceType = require('../models/resource-type'),
      Resource = require('../models/resource'),
      Role = require('../models/role'),
      Entry = require('../models/entry'),
      Company = require('../models/company'),
      Holding = require('../models/holding'),
      UserType = require('../models/user-type'),
      // MIDDLEWARES
      accessControl = require('../middlewares/access-control-managment');


development.use(accessControl);


/* INFO ROUTERS */
development.get('/', (req, res, next) => {
    res.send('Private links');
});


/* RESOURCE */
development.route('/resources')
    .get((req, res, next) => {
        findAll({
            res: res,
            model: Resource,
            type: 'Resource'
        });
    })
    .post((req, res, next) => {
        saveEntry({
            res: res,
            model: Resource,
            type: 'Resource',
            newObj: req.body
        });
    });

/* ONE RESOURCE */
development.route('/resources/:name')
    .get((req, res, next) => {
        findOne({
            res: res,
            model: Resource,
            type: 'Resource',
            filters: {
                name: req.params.name
            }
        });
    })
    .put((req, res, next) => {
        updateEntry({
            res: res,
            model: Resource,
            type: 'Resource',
            update: req.body,
            filters: {
                name: req.params.name
            }
        });
    });


/* RESOURCE TYPES*/
development.route('/resource-types')
    .get((req, res, next) => {
        findAll({
            res: res,
            model: ResourceType,
            type: 'ResourceType'
        });
    })
    .post((req, res, next) => {
        saveEntry({
            res: res,
            model: ResourceType,
            type: 'ResourceType',
            newObj: req.body
        });
    });


/* ONE RESOURCE TYPE*/
development.route('/resource-types/:name')
    .get((req, res, next) => {
        findOne({
            res: res,
            model: ResourceType,
            type: 'ResourceType',
            filters: {
                name: req.params.name
            }
        });
    })
    .put((req, res, next) => {
        updateEntry({
            res: res,
            model: ResourceType,
            type: 'ResourceType',
            filters: {
                name: req.params.name
            },
            update: req.body
        });
    });



/* ROLE */
development.route('/roles')
    .get((req, res, next) => {
        findAll({
            res: res,
            model: Role,
            type: 'Role'
        });
    })
    .post((req, res, next) => {
        saveEntry({
            res: res,
            model: Role,
            type: 'Role',
            newObj: req.body
        });
    });


/* ONE ROLE */
development.route('/roles/:name')
    .get((req, res, next) => {
        findOne({
            res: res,
            model: Role,
            type: 'Role',
            filters: {
                name: req.params.name
            }
        });
    })
    .put((req, res, next) => {
        updateEntry({
            res: res,
            model: Role,
            type: 'Role',
            filters: {
                name: req.params.name
            },
            update: req.body
        });
    });


/* ENTRIES */
development.route('/entries')
    .get((req, res, next) => {
        findAll({
            res: res,
            model: Entry,
            type: 'Entry'
        });
    })
    .post((req, res, next) => {
        saveEntry({
            res: res,
            model: Entry,
            type: 'Entry',
            newObj: req.body
        });
    });


/* ONE ENTRY */
development.route('/entries/:id')
    .get((req, res, next) => {
        findOne({
            res: res,
            model: Entry,
            type: 'Entry',
            filters: {
                '_id': req.params.id
            }
        });
    })
    .put((req, res, next) => {
        updateEntry({
            res: res,
            model: Entry,
            filters: {
                '_id': req.params.id
            },
            type: 'Entry',
            update: req.body
        });
    });


/* Companies */
development.route('/companies')
    .get((req, res, next) => {
        findAll({
            res: res,
            model: Company,
            type: 'Company'
        });
    })
    .post((req, res, next) => {
        saveEntry({
            res: res,
            model: Company,
            type: 'Company',
            newObj: req.body
        });
    });


/* ONE COMPANY */
development.route('/companies/:id')
    .get((req, res, next) => {
        findOne({
            res: res,
            model: Company,
            type: 'Company',
            filters: {
                '_id': req.params.id
            }
        });
    })
    .put((req, res, next) => {
        updateEntry({
            res: res,
            model: Company,
            filters: {
                '_id': req.params.id
            },
            type: 'Company',
            update: req.body
        });
    });


/* HOLDINGS  */
development.route('/holdings')
    .get((req, res, next) => {
        findAll({
            res: res,
            model: Holding,
            type: 'Holding'
        });
    })
    .put((req, res, next) => {
        saveEntry({
            res: res,
            model: Holding,
            type: 'Holding',
            newObj: req.body
        });
    });


/* ONE HOLDING */
development.route('/holdings/:id')
    .get((req, res, next) => {
        findOne({
            res: res,
            model: Holding,
            type: 'Holding',
            filters: {
                '_id': req.params.id
            }
        });
    })
    .put((req, res, next) => {
        updateEntry({
            res: res,
            model: Holding,
            update: req.body,
            filters: {
                '_id': req.params.id
            },
            type: 'Holding'
        });
    });


/* USER TYPES */
development.route('/user-types')
    .get((req, res, next) => {
        findAll({
            res: res,
            model: UserType,
            type: 'UserType'
        });
    })
    .post((req, res, next) => {
        saveEntry({
            res: res,
            model: UserType,
            type: 'UserType',
            newObj: req.body
        });
    });


development.route('/user-types/:name')
    .get((req, res, next) => {
        findOne({
            res: res,
            model: UserType,
            type: 'UserType',
            filters: {
                name: req.param.name
            }
        });
    })
    .put((req, res, next) => {
        updateEntry({
            res: res,
            model: UserType,
            update: req.body,
            filters: {
                name: req.params.name
            },
            type: 'Token'
        });
    });



module.exports = [
    {path: '/managment/release', router: release},
    {path: '/managment/development', router: development},
    {path: '/managment/testing', router: testing}
];
