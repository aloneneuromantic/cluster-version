/*jshint esversion: 6*/
const router = require('express').Router();

// testing route
router.route('/')
    .get((req,res,next) => {
        res.send('INDEX PAGE');
    });

module.exports = {
    path: '/',
    router: router
};
