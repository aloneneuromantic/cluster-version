/* jshint esversion: 6 */
const router = require('express').Router(),
    SettingEntry = require('../models/setting-entry');

// Main setup router
router.get('/',(req,res,next) => {
    SettingEntry.findOne({
            name: 'SetupState'
        })
        .then((r) => {
                if (r === null) {
                    new SettingEntry({
                        name: 'SetupState',
                        value: 0
                    })
                    .save((err)  => {
                        if(err) {
                            res.render('setup', {
                                title: 'Setup: step 0',
                                subtitle: 'Creating records installation course',
                                description: 'Sorry, but we did not manage to save the test record configuration. Please contact your administrator.',
                                status: 'Failed',
                                nextStep: 'step-1'
                            });
                        } else {
                            res.render('setup', {
                                title: 'Setup: step 0',
                                subtitle: 'Creating records installation course',
                                description: 'Congratulating! You started the installation software package "Lobster". This step creates the first entry to the database.',
                                status: 'Success',
                                nextStep: 'step-1'
                            });
                        }
                    });
                } else if(r.value === '0') {
                    res.render('setup', {
                                title: 'Setup: step 0',
                                subtitle: 'Creating records installation course',
                                description: 'Congratulating! You started the installation software package "Lobster". This step creates the first entry to the database.',
                                status: 'Success',
                                nextStep: 'step-1'
                            });
                } else {
                    res.redirect( `/setup/step-${r.value}`);
                }
            })
        .catch((err)  =>  {
                console.log(err);
            });
});


router.route('/step-1')
    .get((req, res, next) => {
        SettingEntry.findOne({
            name: 'SetupState'
            })
        .then((r) => {
                if(r.value === '0' || r.value === '1') {
                    if(r.value === '0') {
                        r.value = '1';
                        r.save()
                        .then((r) => {
                            res.render('setup', {
                                title: 'Setup: step 1',
                                subtitle: 'Creating a host configuration',
                                description: 'Please fill all fields',
                                status: 'In-process',
                                nextStep: 'step-2'
                            });
                        })
                        .catch((err) => {
                            res.render('setup', {
                                title: 'Setup: step 1',
                                subtutle: 'Creating a host configuration',
                                descripton: `Step settings are not saved! Process end with error(s): ${err}. Please try again!`,
                                status: 'In-process',
                                nextStep: 'step-2'
                            });
                        });
                    } else {
                        SettingEntry.findOne({
                            name: 'ServerPort'
                        })
                            .then((r) => {
                                if(r === null) {
                                    res.render('setup', {
                                        title: 'Setup: step 1',
                                        subtitle: 'Creating a host configuration',
                                        desctiption: 'Please fill all fields',
                                        status: 'In-process',
                                        nextStep: 'step-2'
                                    });
                                } else {
                                    res.render('setup', {
                                        title: 'Setup: step 1',
                                        subtitle: 'Creating a host configuration',
                                        description: 'All settings saved! Go to the next step',
                                        status: 'Success',
                                        nextStep: 'step-2'
                                    });;
                                }
                            });
                    }
                } else {
                    res.redirect(`/setup/step-${r.value}`);
                }
            })
        .catch((err) => {
                console.log(err);
            });
    })
    .post((req, res, next) => {
        let port  = new SettingEntry({
            name: 'ServerPort',
            value: req.body.serverPort
        }),
            address = new SettingEntry({
                name: 'ServerAddress',
                value: req.body.serverAddress
            });
        SettingEntry.findOne({
            name: 'ServerPort'
        })
            .then((r) => {
                if(r === null) {
                    Promise.all([
                        port.save(),
                        address.save()
                    ])
                        .then((r) =>{
                            res.render('setup', {
                                title: 'Setup: step-1',
                                subtutle: 'Create a host configuration',
                                description: 'Congratulation! All configs for host saved. Blease go to the next step!',
                                status: 'Success',
                                nextStep: 'step-2'
                            });
                        })
                        .catch((err) => {
                            res.render('setup', {
                                title: 'Setup: step-1',
                                subtitle: 'Create a host configuration',
                                description: `When app try save data in MongoDB, process break with error: {$err}`,
                                status: 'Failed',
                                nextStep: 'step-2'
                            });
                        });

                }
            })
            .catch((err) => {
                console.log('ERROR WHEN FIND: ', err);
            });
    });


router.route('/step-2')
    .get((req, res, next) => {
        SettingEntry.findOne({
            name: 'SetupState'
        })
            .then((e) =>{
                if(e === null) {
                    res.redirect('/setup');
                } else {
                    if(e.value === '1' || e.value === '2') {
                        if(e.value === '1') {
                            e.value = '2';
                            e.save()
                                .then((r) => {
                                    res.render('setup', {
                                        title: 'Setup: step-2',
                                        subtitle: 'Create a MongoDB settings',
                                        description: 'Please fill all fields',
                                        status: 'In-process',
                                        nextStep: 'step-3'
                                    });
                                })
                                .catch((err) => {
                                    res.render('setup', {
                                        title: 'Setup: step-2',
                                        subtitle: 'Create a MongoDB settings',
                                        description: `When app try saved change in database process break with error ${err}`,
                                        status: 'Failed',
                                        nextStep: 'step-3'
                                    });
                                });
                        }
                    } else {
                        res.redirect(`/setup/step-${e.value}`);
                    }
                }
            })
            .catch((err) => {
                console.log('ERROR WHEN FIND ENTRY: ', err);
            });
    })
    .post((req, res, next) => {
        SettingEntry.findOne({
        name: 'SetupState',
    })
        .then((r) => {
            if(r === null) {
                res.redirect('/setup');
            } else {
                if(r.value === '1' || r.value === '2') {
                    SettingEntry.findOne({
                        name: 'ServerPort'
                    })
                        .then((r) => {
                            if(r !== null) {
                                let mongoHost = new SettingEntry({
                                        name: 'MongoHost',
                                        value: req.body.mongoHost
                                    }),
                                    mongoPort = new SettingEntry({
                                        name: 'MongoPort',
                                        value: req.body.mongoPort
                                    }),
                                    adminLogin = new SettingEntry({
                                        name: 'AdminLogin',
                                        value: req.body.adminLogin
                                    });
                                if(req.body.adminPassword !== req.body.adminPasswordRepeat){
                                    res.render('setup', {
                                        title: 'Setup: step-2',
                                        subtitle: 'Create a MongoDB settings',
                                        description: 'You repeat passwords do not equals',
                                        status: 'Failed',
                                        nextStep: 'step-3'
                                    });
                                } else {
                                    let mongoPassword = new SettingEntry({
                                            name: 'MongoPassword',
                                            value: req.body.adminPassword
                                    });
                                    Promise.all([
                                        mongoHost.save(),
                                        mongoPort.save(),
                                        adminLogin.save(),
                                        mongoPassword.save()
                                    ])
                                        .then((p) => {
                                            res.render('setup', {
                                                title: 'Setup: step-2',
                                                subtitle: 'Create a MongoDB settings',
                                                description: 'All entries saved! Go to the next step!',
                                                status: 'Success',
                                                nextStep: 'step-3'
                                            });
                                        })
                                        .catch((err) => {
                                            res.render('setup', {
                                                title: 'Setup: step-2',
                                                subtitle: 'Create a MongoDB settings',
                                                desription: `When app save entries process break with error: ${err}`,
                                                status: 'Failed',
                                                nextStep: 'step-3'
                                            });
                                        });
                                }
                            } else {
                                res.redirect('/setup');
                            }
                        })
                        .catch((err) => {
                            console.log('WHEN TRY FINDING, ERROR: ', err);
                        });
                } else {
                    res.redirect(`/setup/step-${r.value}`);
                }
            }
        })
        .catch((err) =>{
            console.log('ERROR WHEN FINDING: ', err);
        });
    });

router.route('/step-3')
    .get((req, res, next) => {
        SettingEntry.findOne({
            name: 'SetupState'
        })
            .then((r) => {
                if(r.value === '2' || r.value === '3') {
                    SettingEntry.findOne({
                        name: 'MongoPassword'
                    })
                        .then((mp) => {
                            if(mp === null) {
                                res.redirect(`/setup/step-${r.value}`);
                            } else {
                                if(r.value === '3') {
                                    res.render('setup', {
                                        title: 'Setup: step-3',
                                        subtitle: 'End of setup LobsterJS',
                                        description: 'All satting saves. Use admin profile or MongoExpress to edit entries',
                                        status: 'Success',
                                        nextStep: null,
                                    });
                                } else {
                                    r.value = '3';
                                    r.save()
                                        .then((s) => {
                                            res.render('setup', {
                                                title: 'Setup: step-3',
                                                subtitle: 'End of setup LobsterJS',
                                                description: 'Final step. Push down server LobsterJS & restart this',
                                                status: 'Success',
                                                nextStep: 'end'
                                            });
                                        })
                                        .catch((serr) => {
                                            res.render('step', {
                                                title: 'Setup: step-3',
                                                subtitle: 'End of setup LobsterJS',
                                                description: `When app try save process break with error`,
                                                status: 'Success',
                                                nextStep:'end'

                                            });
                                        });
                                }
                            }
                        })
                        .catch((rj) => {
                            console.log('ERROR! WHEN FINDING: ', rj);
                        });
                } else {
                    res.redirect(`/setup/step-${r.value}`);
                }
            })
            .catch((err) => {
                console.log('ERROR! MESSAGE: ', err);
            });
    });

module.exports = {
    path: '/setup',
    router: router
};
 
