/**
 * @api {get} /tokens Request list of tokens
 * @apiName GetTokens
 * @apiGroup Development
 * @apiVersion 0.0.1
 *
 * @apiSampleRequest off
 *
 * @apiPermission Authorized users only
 *
 * @apiParam {String} token JWT user token
 *
 * @apiHeader {String} referrer
 * Login resource authorization system
 *
 * @apiSuccess {Array} data An array of tokens
 * @apiSuccess {Object} error An empty object with errors
 * @apiSuccess {Object} meta An object with additional data (counter)
 *
 * @apiSuccessExample Success-Response:
 * HTTP 200 OK
 * {
 *   "data": [
 *            {
 *                "id": "58789b22dc915610bffa8f3e",
 *                "type": "Token",
 *                "attributes": {
 *                "_id": "58789b22dc915610bffa8f3e",
 *                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc1Vuc2FmZSI6ZmFsc2UsInVzZXJuYW1lIjoiODk5Njk0NjQ2MjkiLCJmaW8iOiLQmNCy0LDQvdC-0LIg0JjQstCw0L0g0JjQstCw0L3QvtCy0LjRhyIsImlhdCI6MTQ4NDI5OTA0MiwiZXhwIjoxNDg0Mzg1NDQyfQ.yqFait4TFRuJsL1S6ZhoVomvnR5lBT13iS1Jax5TEnA",
 *                "user": "5864bdfb9885ad5e9b73aff8",
 *                "__v": 0,
 *                "createdAt": "2017-01-13T09:17:22.195Z"
 *            }
 *          ],
 *    "errors": {},
 *    "meta": {
 *             "count": 1
 *      }
 * }
 *
 * @apiError TokentNotTransfered 404 Not transferred token
 * @apiError ReferrerNotTransfered 404 Not transfered resource token
 *
 * @apiErrorExample TokentNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 * @apiErrorExample ReferrerNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 */



/**
 * @api {get} /tokens/:token Get all atribute one token
 * @apiName GetTokenInfo
 * @apiGroup Development
 * @apiVersion 0.0.1
 *
 * @apiSampleRequest off
 *
 * @apiPermission Authorized users only
 *
 * @apiParam {String} token JWT user token
 *
 * @apiHeader {string} referrer
 * Login resource authorization system
 *
 * @apiSuccess {Array} data An array of token attributes
 * @apiSuccess {Object} error An empty object with errors
 * @apiSuccess {Object} meta An object with additional data (counter)
 *
 * @apiSuccessExample Success-Response:
 * HTTP 200 OK
 * {
 *   {
 *     "data": {
 *        "type": "Token",
 *        "attributes": {
 *          "id": "587d99b2b0c5d976bffe77b9",
 *          "user": "5864bdfb9885ad5e9b73aff8",
 *          "createdAt:": "Tue Jan 17 2017 09:12:34 GMT+0500 (+05)",
 *          "endingAt": " "
 *        }
 *     },
 *     "errors": {},
 *     "meta": {}
 *   }
 * }
 *
 * @apiError TokentNotTransfered 404 Not transferred token
 * @apiError ReferrerNotTransfered 404 Not transfered resource token
 *
 * @apiErrorExample TokentNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 * @apiErrorExample ReferrerNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 */



/**
 * @api {post} /tokens/:token Check token to authorization
 * @apiName CheckToken
 * @apiGroup Development
 * @apiVersion 0.0.1
 *
 * @apiSampleRequest off
 *
 * @apiPermission Authorized users only
 *
 * @apiParam {String} token JWT user token
 *
 * @apiHeader {string} referrer
 * Login resource authorization system
 *
 * @apiSuccess {Array} data An array of token attributes
 * @apiSuccess {Object} error An empty object with errors
 * @apiSuccess {Object} meta An object with additional data (counter)
 *
 * @apiSuccessExample Success-Response:
 * HTTP 200 OK
 * {
 *    "data": {
 *         "type": "Token",
 *         "attributes": {
 *         "id": "587e0cc1277cd23513ab211b",
 *         "user": "587ddc5fa558914e64e9c026",
 *         "createdAt:": "Tue Jan 17 2017 17:23:29 GMT+0500 (+05)",
 *         "endingAt": " "
 *       }
 *    },
 *    "errors": {},
 *    "meta": {}
 * }
 *
 * @apiError TokentNotTransfered 404 Not transferred token
 * @apiError ReferrerNotTransfered 404 Not transfered resource token
 *
 * @apiErrorExample TokentNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 * @apiErrorExample ReferrerNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 */


/**
 * @api {get} /users Request list of Users
 * @apiName GetUsers
 * @apiGroup Development
 * @apiVersion 0.0.1
 *
 * @apiSampleRequest off
 *
 * @apiPermission Authorized users only
 *
 * @apiParam {String} token JWT user token
 *
 * @apiHeader {string} referrer
 * Login resource authorization system
 *
 * @apiSuccess {Array} data An array of tokens
 * @apiSuccess {Object} error An empty object with errors
 * @apiSuccess {Object} meta An object with additional data (counter)
 *
 * @apiSuccessExample Success-Response:
 * HTTP 200 OK
 * {
 *   "data": [
 *             {
 *               "id": "5862211f9308113f11e0d0fe",
 *               "type": "User",
 *               "attributes": {
 *                               "_id": "5862211f9308113f11e0d0fe",
 *                               "password": "123123213",
 *                               "fio": "Троллкина Лолка Васильевна",
 *                               "city_id": 1,
 *                               "customer_type": "b2c",
 *                               "phone": "89969464620",
 *                               "type": "5860e012c1bdd31318a909ae",
 *                               "company": "5860d7efde603779d12580a6",
 *                               "__v": 0,
 *                               "tokens": null,
 *                               "contacts": null,
 *                               "updated_at": "2016-12-27T08:06:55.141Z",
 *                               "managers": null,
 *                               "price_type": "price",
 *                               "is_registration_complete": false,
 *                               "can_see_logic_reports": false,
 *                               "is_verified_emai": false,
 *                               "is_verified_phone": false,
 *                               "sale_used": false,
 *                               "full_fio": true
 *                             }
 *              }
 *          ],
 *    "errors": {},
 *    "meta": {
 *             "count": 1
 *      }
 * }
 *
 * @apiError TokentNotTransfered 404 Not transferred token
 * @apiError ReferrerNotTransfered 404 Not transfered resource token
 *
 * @apiErrorExample TokentNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 * @apiErrorExample ReferrerNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 */

/**
 * @api {post} /users Create new User
 * @apiName CreateUser
 * @apiGroup Development
 * @apiVersion 0.0.1
 *
 * @apiSampleRequest off
 *
 * @apiPermission Authorized users only
 *
 * @apiParam {String} phone User login (shoul be a string, usually the user's phone )
 *
 * @apiHeader {string} referrer
 * Login resource authorization system
 *
 * @apiSuccess {Array} data An object with status operation & message with description
 * @apiSuccess {Object} error An empty object with errors
 * @apiSuccess {Object} meta An empry object
 *
 * @apiSuccessExample Success-Response:
 * HTTP 200 OK
 * {
 *     "data": {
 *         "status": "success",
 *         "message": "Actions with User carried out successfully"
 *      },
 *      "errors": {},
 *      "meta": {}
 * }
 *
 * @apiError TokentNotTransfered 404 Not transferred token
 * @apiError ReferrerNotTransfered 404 Not transfered resource token
 * @apiError AttributesNotSend 501 Not transfered some attributes
 *
 * @apiErrorExample AttributesNotSend:
 * HTTP 500 Internal server error
 * {
 *   "name": "AssertionError",
 *   "actual": false,
 *   "expected": true,
 *   "operator": "==",
 *   "message": "key must be a string or buffer",
 *   "generatedMessage": false
 * }
 * @apiErrorExample TokentNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 * @apiErrorExample ReferrerNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 */


/**
 * @api {get} /users/:phone Get user information
 * @apiName GetUser
 * @apiGroup Development
 * @apiVersion 0.0.1
 *
 * @apiSampleRequest off
 *
 * @apiPermission Authorized users only
 *
 * @apiParam {String} phone User login (shoul be a string, usually the user's phone)
 *
 * @apiHeader {string} referrer
 * Login resource authorization system
 *
 * @apiSuccess {Array} data An object with status operation & message with description
 * @apiSuccess {Object} error An empty object with errors
 * @apiSuccess {Object} meta An empry object
 *
 * @apiSuccessExample Success-Response:
 * HTTP 200 OK
 * {
 *     "data": {
 *            "id": "5864bdfb9885ad5e9b73aff8",
 *            "type": "User",
 *           	"attributes": {
 *                  "_id": "5864bdfb9885ad5e9b73aff8",
 *                  "phone": "89969464629",
 *                  "password": "gxvWyD/1MNO0MS4Av2QjeVTBlBTn8Ouutiw1/RBSKpw=",
 *                  "fio": "Иванов Иван Иванович",
 *                  "type": "587cadf1618c40564d447839",
 *                  "__v": 71,
 *                  "tokens": [
 *                      "5875b6619d0906295bc96e40"
 *                  ],
 *                  "contacts": [],
 *                  "updated_at": "2016-12-29T07:40:43.586Z",
 *                  "managers": [],
 *                  "price_type": "price",
 *                  "is_registration_complete": false,
 *                  "can_see_logic_reports": false,
 *                  "is_verified_emai": false,
 *                  "is_verified_phone": false,
 *                  "sale_used": false,
 *                  "full_fio": false
 *         }
 *      },
 *      "errors": {},
 *      "meta": {}
 * }
 *
 * @apiError TokentNotTransfered 404 Not transferred token
 * @apiError ReferrerNotTransfered 404 Not transfered resource token
 * @apiError LoginError 409 Phone/login not find
 *
 * @apiErrorExample AttributesNotSend:
 * HTTP 409 Conflict
 * {
 *    "data": {},
 *    "errors": {
 *       "type": "User",
 *       "message": "Application can not find object with type User"
 *    },
 *    "meta": {}
 * }
 * @apiErrorExample TokentNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 * @apiErrorExample ReferrerNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 */
/**
 * @api {put} /users/:phone Save edit user information
 * @apiName EditUser
 * @apiGroup Development
 * @apiVersion 0.0.1
 *
 * @apiSampleRequest off
 *
 * @apiPermission Authorized users only
 *
 * @apiParam {String} phone User login (shoul be a string, usually the user's phone)
 * @apiParam {Object} json JSON with user attributes 
 *
 * @apiHeader {string} referrer
 * Login resource authorization system
 *
 * @apiSuccess {Array} data An object with status operation & message with description
 * @apiSuccess {Object} error An empty object with errors
 * @apiSuccess {Object} meta An empry object
 *
 * @apiSuccessExample Success-Response:
 * HTTP 200 OK
 * {
 *    "data": {
 *       "status": "success",
 *       "message": "Actions with User carried out successfully"
 *    },
 *    "errors": {},
 *    "meta": {}
 * }
 *
 * @apiError TokentNotTransfered 404 Not transferred token
 * @apiError ReferrerNotTransfered 404 Not transfered resource token
 * @apiError LoginError 409 Phone/login not find
 *
 * @apiErrorExample AttributesNotSend:
 * HTTP 409 Conflict
 * {
 *    "data": {},
 *    "errors": {
 *       "type": "User",
 *       "message": "Application can not find object with type User"
 *    },
 *    "meta": {}
 * }
 * @apiErrorExample TokentNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 * @apiErrorExample ReferrerNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 */
/**
 * @api {post} /login Request to authorization
 * @apiName Authorize
 * @apiGroup Development
 * @apiVersion 0.0.1
 *
 * @apiSampleRequest off
 *
 * @apiPermission For all  users
 *
 * @apiParam {String} token JWT user token
 * @apiParam {String} phone Phone user (user login)
 * @apiParam {String} password Password of user (not hashed)
 *
 * @apiSuccess {Array} data An array of tokens
 * @apiSuccess {Object} error An empty object with errors
 * @apiSuccess {Object} meta An object with additional data (counter)
 *
 * @apiHeader {String} referrer
 * Login resource authorization system
 *
 * @apiSuccessExample Success-Response:
 * HTTP 200 OK
 * {
 *     "data": {
 *            "id": "5864bdfb9885ad5e9b73aff8",
 *            "type": "User",
 *           	"attributes": {
 *                  "_id": "5864bdfb9885ad5e9b73aff8",
 *                  "phone": "89969464629",
 *                  "password": "gxvWyD/1MNO0MS4Av2QjeVTBlBTn8Ouutiw1/RBSKpw=",
 *                  "fio": "Иванов Иван Иванович",
 *                  "type": "587cadf1618c40564d447839",
 *                  "__v": 71,
 *                  "tokens": [
 *                      "5875b6619d0906295bc96e40"
 *                  ],
 *                  "contacts": [],
 *                  "updated_at": "2016-12-29T07:40:43.586Z",
 *                  "managers": [],
 *                  "price_type": "price",
 *                  "is_registration_complete": false,
 *                  "can_see_logic_reports": false,
 *                  "is_verified_emai": false,
 *                  "is_verified_phone": false,
 *                  "sale_used": false,
 *                  "full_fio": false
 *         }
 *      },
 *      "errors": {},
 *      "meta": {}
 * }
 *
 * @apiError IncorrectData 404 Incorrect data, user not authorized
 *
 * @apiErrorExample IncorrectData:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 *
 */
/**
 * @api {post} /ulogin Request to authorization without password (unsafe method)
 * @apiName UnsafeAuthorize
 * @apiGroup Development
 * @apiVersion 0.0.1
 *
 * @apiSampleRequest off
 *
 * @apiPermission For all users
 *
 * @apiParam {String} phone Phone user (user login)
 *
 * @apiSuccess {Array} data An array of tokens
 * @apiSuccess {Object} error An empty object with errors
 * @apiSuccess {Object} meta An object with additional data (counter)
 *
 * @apiHeader {String} referrer
 * Login resource authorization system
 *
 * @apiError IncorrectData 501 Incorrect data, user not authorized
 *
 * @apiSuccessExample Success-Response:
 * HTTP 200 OK
 * {
 *    "data": {
 *        "id": "5864bdfb9885ad5e9b73aff8",
 *        "type": "User",
 *        "attributes": {
 *              "_id": "5864bdfb9885ad5e9b73aff8",
 *              "phone": "89969464629",
 *              "password": "gxvWyD/1MNO0MS4Av2QjeVTBlBTn8Ouutiw1/RBSKpw=",
 *              "fio": "Иванов Иван Иванович",
 *              "type": "587cadf1618c40564d447839",
 *              "__v": 90,
 *              "tokens": [
 *                     "5875b6619d0906295bc96e40",
 *              ],
 *              "contacts": [],
 *              "updated_at": "2016-12-29T07:40:43.586Z",
 *              "managers": [],
 *              "price_type": "price",
 *              "is_registration_complete": false,
 *              "can_see_logic_reports": false,
 *              "is_verified_emai": false,
 *              "is_verified_phone": false,
 *              "sale_used": false,
 *              "full_fio": false
 *        }
 *     },
 *     "errors": {},
 *     "meta": {
 *       "isUnsafe": true,
 *       "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc1Vuc2FmZSI6dHJ1ZSwidXNlcm5hbWUiOiI4OTk2OTQ2NDYyOSIsImZpbyI6ItCY0LLQsNC90L7QsiDQmNCy0LDQvSDQmNCy0LDQvdC-0LLQuNGHIiwicmVmZXJyZXIiOiJ0ZXN0LWxvZ2luIiwidHlwZSI6IjU4N2NhZGYxNjE4YzQwNTY0ZDQ0NzgzOSIsImlhdCI6MTQ4NDczMjY4MywiZXhwIjoxNDg0ODE5MDgzfQ.k7K3LLjuN_gUp26ZB6OTla4gwNKuVbgyY3Em2beRGfk"
 *     }
 * }
 *
 * @apiErrorExample IncorrectData:
 * HTTP 501 Not implemented
 * {
 *     "data": [],
 *     "errors": {
 *         "type": "User",
 *         "message": "Empty main data with type User"
 *     },
 *     "meta": {
 *         "data": [],
 *         "isError": false,
 *         "type": "User"
 *     }
 * }
 *
 */
/**
 * @api {post} /logout Logout user token
 * @apiName Logout
 * @apiGroup Development
 * @apiVersion 0.0.1
 *
 * @apiSampleRequest off
 *
 * @apiPermission Authorized users only
 *
 * @apiParam {String} token JWT user token
 *
 * @apiHeader {String} referrer
 * Login resource authorization system
 *
 * @apiSuccess {Array} data An array of tokens
 * @apiSuccess {Object} error An empty object with errors
 * @apiSuccess {Object} meta An object with additional data (counter)
 *
 * @apiSuccessExample Success-Response:
 * HTTP 200 OK
 * {
 *     "data": {
 *         "status": "success",
 *         "message": "Actions with User carried out successfully"
 *      },
 *      "errors": {},
 *      "meta": {}
 * }
 *
 * @apiError TokentNotTransfered 404 Not transferred token
 * @apiError ReferrerNotTransfered 404 Not transfered resource token
 *
 * @apiErrorExample TokentNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 * @apiErrorExample ReferrerNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 */
/**
 * @api {post} /register Register new User (recomended)
 * @apiName RegisterUser
 * @apiGroup Development
 * @apiVersion 0.0.1
 *
 * @apiSampleRequest off
 *
 * @apiPermission For all users
 *
 * @apiParam {String} phone User login (shoul be a string, usually the user's phone)
 * @apiParam {String} password User password (not hashed)
 *
 * @apiHeader {string} referrer
 * Login resource authorization system
 *
 * @apiSuccess {Array} data An object with status operation & message with description
 * @apiSuccess {Object} error An empty object with errors
 * @apiSuccess {Object} meta An empry object
 *
 * @apiSuccessExample Success-Response:
 * HTTP 200 OK
 * {
 *     "data": {
 *         "status": "success",
 *         "message": "Actions with User carried out successfully"
 *      },
 *      "errors": {},
 *      "meta": {}
 * }
 *
 * @apiError TokentNotTransfered 404 Not transferred token
 * @apiError ReferrerNotTransfered 404 Not transfered resource token
 * @apiError AttributesNotSend 501 Not transfered some attributes
 *
 * @apiErrorExample AttributesNotSend:
 * HTTP 500 Internal server error
 * {
 *   "name": "AssertionError",
 *   "actual": false,
 *   "expected": true,
 *   "operator": "==",
 *   "message": "key must be a string or buffer",
 *   "generatedMessage": false
 * }
 * @apiErrorExample TokentNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 * @apiErrorExample ReferrerNotTransfered:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 */
/**
 * @api {post} /lor Request to authorization or register user
 * @apiName Authorize
 * @apiGroup Development
 * @apiVersion 0.0.1
 *
 * @apiSampleRequest off
 *
 * @apiPermission For all  users
 *
 * @apiParam {String} token JWT user token
 * @apiParam {String} phone Phone user (user login)
 * @apiParam {String} password Password of user (not hashed)
 *
 * @apiSuccess {Array} data An array of tokens
 * @apiSuccess {Object} error An empty object with errors
 * @apiSuccess {Object} meta An object with additional data (counter)
 *
 * @apiHeader {String} referrer
 * Login resource authorization system
 *
 * @apiSuccessExample Success-Response (login):
 * HTTP 200 OK
 * {
 *     "data": {
 *            "id": "5864bdfb9885ad5e9b73aff8",
 *            "type": "User",
 *           	"attributes": {
 *                  "_id": "5864bdfb9885ad5e9b73aff8",
 *                  "phone": "89969464629",
 *                  "password": "gxvWyD/1MNO0MS4Av2QjeVTBlBTn8Ouutiw1/RBSKpw=",
 *                  "fio": "Иванов Иван Иванович",
 *                  "type": "587cadf1618c40564d447839",
 *                  "__v": 71,
 *                  "tokens": [
 *                      "5875b6619d0906295bc96e40"
 *                  ],
 *                  "contacts": [],
 *                  "updated_at": "2016-12-29T07:40:43.586Z",
 *                  "managers": [],
 *                  "price_type": "price",
 *                  "is_registration_complete": false,
 *                  "can_see_logic_reports": false,
 *                  "is_verified_emai": false,
 *                  "is_verified_phone": false,
 *                  "sale_used": false,
 *                  "full_fio": false
 *         }
 *      },
 *      "errors": {},
 *      "meta": {}
 * }
 * @apiSuccessExample Success-Response (register):
 * HTTP 200 OK
 * {
 *     "data": {
 *         "status": "success",
 *         "message": "Actions with User carried out successfully"
 *      },
 *      "errors": {},
 *      "meta": {}
 * }
 *
 *
 * @apiError IncorrectData 404 Incorrect data, user not authorized
 *
 * @apiErrorExample IncorrectData:
 * HTTP 404 Not found
 * {
 *    "data": {},
 *    "errors": {
 *      "type": "Resource",
 *      "message": "Application can not find object with type Resource"
 *    },
 *    "meta": {}
 * }
 *
 */
