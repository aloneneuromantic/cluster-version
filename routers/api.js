/* jshint esversion:6 */
const release = require('express').Router(),
      testing = require('express').Router(),
      development = require('express').Router(),
      // HELPERS
      findAll = require('../helpers/find-all'),
      findOne = require('../helpers/find-one'),
      saveEntry = require('../helpers/save-enty'),
      updateEntry = require('../helpers/update-entry'),
      validatePassword = require('../helpers/validate-password'),
      generatePassword = require('../helpers/generate-password'),
      authUser = require('../helpers/auth-user'),
      logoutUser = require('../helpers/logout-user'),
      authUnsafe = require('../helpers/auth-unsafe'),
      checkToken = require('../helpers/check-token'),
      regOrAuth = require('../helpers/register-or-login'),
      onlyAuthUnsafe = require('../helpers/only-auth-unsafe'),
      answerJSON = require('../helpers/answer-json'),
      getFromRedis = require('../helpers/get-from-redis'),
      answerBuilder = require('../helpers/json-builder'),
      // MIDDLEWARES
      accessControl = require('../middlewares/access-control-api'),
      // NODE MODULES
      jwt = require('jsonwebtoken'),
      // MODELS
      Token = require('../models/token'),
      User = require('../models/user'),
      //DATABASE
      client = require('redis').createClient(),
      unlessRouter = require('../helpers/unless-router'),
      withoutControl = [
          '/login',
          '/register',
          '/lor',
          '/ulogin'
      ];

development.use(unlessRouter(withoutControl, accessControl));

/* INFO ROUTERS */
development.route('/')
    .get((req, res, next) => {
        answerBuilder({
            res: res,
            data: {
                state: 'not-support'
            }
        });
    });

/* TOKENS */
development.route('/tokens')
    .get((req, res, next) => {
        findAll({
            res: res,
            model: Token,
            type: 'Token'
        });
    })
    .post((req, res, next) => {
        answerBuilder({
            res: res,
            data: {
                state: 'not-support'
            }
        });
    });


/* ONE TOKEN */
development.use('/tokens/:token', (req, res, next) => {
    getFromRedis({
        req: res,
        client: client,
        hash: req.params.token,
        type: 'Token'}).then((json) => {
            answerBuilder({
                res: res,
                data: {
                    type: 'Token',
                    exhaust: json
                }
            });
        })
        .catch((error) => {
            next();
        });
});
development.route('/tokens/:token')
    .get((req, res, next) => {
        findOne({
            res: res,
            model: Token,
            type: 'Token',
            filters: {
                token: req.param.token
            }
        });
    })
    .post((req, res, next) => {
        checkToken({
            req: res,
            res: res,
            type: 'Token'
        });
    })
    .put((req, res, next) => {
        updateEntry({
            res: res,
            model: Token,
            update: req.body,
            filters: {
                token: req.params.token
            },
            type: 'Token'
        });
    });


/* USER TABLE API */
development.route('/users')
    .get((req, res, next) => {
        findAll({
            res: res,
            model: User,
            type: 'User'
        });
    })
    .post((req, res, next) => {
        const reqObj = req.body,
              hash = generatePassword(reqObj.password);
        reqObj.password = hash;
        saveEntry({
            res: res,
            model: User,
            type: 'User',
            newObj: reqObj
        });

    });


/* ONE USER */
development.route('/users/:phone')
    .get((req, res, next) => {
        findOne({
            res: res,
            model: User,
            type: 'User',
            filters: {
                phone: req.params.phone
            }
        });
    })
    .put((req, res, next) => {
        updateEntry({
            res: res,
            model: User,
            filters: {
                phone: req.params.phone
            },
            type: 'User',
            update: req.body
        });
    });


/* LOGIN */
development.route('/login')
    .post((req, res, next) => {
        authUser({
            req: req,
            res: res,
            client: client
        });
    })
    .get((req, res, next) => {
        answerBuilder({
            res: res,
            data: {
                state: 'not-support'
            }
        });
    });


/* UNSAFE LOGIN */
development.route('/ulogin')
    .post((req, res, next) => {
        onlyAuthUnsafe({
            res: res,
            client: client,
            phone: req.body.phone,
            ua: req.body.userAgent
        });
    })
    .get((req, res, next) => {
        answerBuilder({
            res: res,
            data: {
                state: 'not-support'
            }
        });
    });


/* LOGOUT */
development.route('/logout')
    .get((req, res, next) => {
        answerBuilder({
            res: res,
            data: {
                state: 'not-support'
            }
        });
    })
    .post((req, res, next) => {
        logoutUser({
            req: req,
            res: res,
            type: 'Token'
        });
    });

/* REGISTER */
development.route('/register')
    .get((req, res, next) => {
        answerBuilder({
            res: res,
            data: {
                state: 'not-support'
            }
        });
    })
    .post((req, res, next) => {
        const reqObj = req.body,
              hash = generatePassword(reqObj.password);
        reqObj.password = hash;
        saveEntry({
            res: res,
            model: User,
            type: 'User',
            newObj: reqObj
        });
    });


/* LOGIN OR REGISTER */
development.route('/lor')
    .get((req, res, next) => {
        answerBuilder({
            res: res,
            data: {
                state: 'not-support'
            }
        });
    })
    .post((req, res, next) => {
        regOrAuth({
            res: res,
            req: req,
            client: client
        });
    });


/* EXPORTS ALL ROUTERS */
module.exports = [
    {path: '/api/release', router: release},
    {path: '/api/development', router: development},
    {path: '/api/testing', router: testing}
];
