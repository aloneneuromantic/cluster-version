// Objects with server-settings
let mongoSettings = {
    port: '27017',
    host: 'localhost',
    user: 'admin',
    password: 'none'
};

module.exports = mongoSettings;