// Objects with main server settings
let settings = {
    port: '3000',
    host: 'localhost'
};

module.exports = settings;
