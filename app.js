/*jshint esversion: 6*/

//// Main app constant
const FileStreamRotator = require('file-stream-rotator'),
	    express = require('express'),
	    fs = require('fs'),
	    path = require('path'),
	    favicon = require('serve-favicon'),
	    morgan = require('morgan'),
	    cookieParser = require('cookie-parser'),
	    bodyParser = require('body-parser'),
      mongoExpressConfig = require('./configs/mongo-express'),
	    app = express(),
      cors = require('./middlewares/cors'),
      mongoExpress = require('mongo-express/lib/middleware'),
	    logDirectory = path.join(__dirname, 'log'),
	    mongoose = require('mongoose'),
	    utils = require('./utils'),
      cache = require('express-redis-cache')(),
      router = express.Router();

mongoose.Promise = Promise;

fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
// create a rotating write stream
let accessLogStream = FileStreamRotator.getStream({
	date_format: 'YYYYMMDD',
	filename: path.join(logDirectory, 'access-%DATE%.log'),
	frequency: 'daily',
	verbose: false
});
//// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

///////TEST
// a middleware sub-stack shows request info for any type of HTTP request to the /user/:id path
router.use('/user/:id', function(req, res, next) {
    console.log('Request URL:', req.originalUrl);
    next();
}, function (req, res, next) {
    console.log('Request Type:', req.method);
    next();
});



////// Set cors middleware
app.use(cors);

/* Morgan */
app.use(morgan('combined', {
	stream: accessLogStream
}));

utils('get-all-settings')
	  .then((settings) => {
        let MongoSettings = {},
            max = settings.length,
            iterator = 1;
        settings.forEach((entry) => {
            if(entry.name !== 'ServerPort' &&
               entry.name !== 'ServerAddress') {
                MongoSettings[entry.name] = entry.value;
            }
            if(iterator === max) {
                if(MongoSettings.SetupState === '3') {
                    let cs = null;

                    if(MongoSettings.MongoPassword !== 'none') {
                        cs = `mongodb://${MongoSettings.AdminLogin}:${MongoSettings.MongoPassword}@${MongoSettings.MongoHost}:${MongoSettings.MongoPort}/db`;
                        cs = `mongodb://${MongoSettings.MongoHost}:${MongoSettings.MongoPort}/db`;

                    } else {
                        cs = `mongodb://${MongoSettings.MongoHost}:${MongoSettings.MongoPort}/db`;
                    }
                    setTimeout(()=>{
                        mongoose.connect(cs)
                            .then(() => {
                                console.log('Mongoose connected!');
                            })
                            .catch((error) => {
                                console.log(error);
                            });
                    }, 6000);
                }

            }
            iterator += 1;
        });
		})
	  .catch((error) => {
			  console.log(error);
  		  mongoose.connect('mongodb://localhost:27017/db');
		});
//// Mongo-Express
app.use('/mongo-admin', mongoExpress(mongoExpressConfig));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// Routers
app.use('/apidoc', express.static('apidoc'));
let obj = null,
    lst = ['index', 'setup', 'api','managment'],
    max = lst.length,
    iterator = 1;
if(max  === 0) {
    console.log('ERROR!!!');
} else {
    lst.forEach((r) => {
        obj = require(`./routers/${r}`);
        if(max !== iterator) {
            if(Array.isArray(obj) === true) {
                let xmax = obj.length,
                    xterator = 1;
                    obj.forEach((x) => {
                        if(xterator === xmax) {
                            iterator += 1;
                        } else {
                            console.log(`Build router ${r}`);
                            app.use(x.path, x.router);
                        }
                    });
            } else {
                app.use(obj.path, obj.router);
            }
        } else {
            console.log('123123123');
        }
    });
}


// catch 404 and forward to error handler
app.use((req, res, next) => {
	let err = new Error('Not Found');
	err.status = 404;
	next(err);
});


////// error handler
app.use((err, req, res, next) => {
// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

// render the error page
	res.status(err.status || 500);
	res.send(err);
});


let getApplication = function(cs) {
	return app;
};

module.exports = getApplication;
