/* jshint esversion:6 */
const answerJSON = require('../helpers/answer-json'),
      buildJSON = require('../helpers/build-json'),
      Resource = require('../models/resource.js'),
      secretWord = require('../configs/secret-word'),
      jwt = require('jsonwebtoken'),
      validateRoleAndType = require('../helpers/validate-role-and-type-private.js');

let accessControl = (req, res, next) => {
    let reqRes = req.headers.referrer || req.query.referrer || req.cookies.referrer || req.headers.referrer || undefined,
        token = req.body.token || req.query.token || req.coockies.token || req.headers['x-accecss-token'] || undefined;
    if(req.headers.referrer === undefined && token === undefined) {
        answerJSON('resource-not-support')
            .then((json) => {
                res.status(501).json(json);
            })
            .catch((error) => {
                res.status(404).json(error);
            });
    } else {
        Resource.findOne({
            name: reqRes
        })
            .then((resource) => {
                if(resource !== null){
                    jwt.verify(token, secretWord, (error, decoded) => {
                        if(error !== null) {
                            buildJSON(error, false, 'Decode')
                                .then((json) => {
                                    res.status(404).json(json);
                                })
                                .catch((error) => {
                                    res.status(501).json(error);
                                });
                        } else {
                            if(decoded.referrer !== reqRes) {
                                buildJSON([], false, 'Resource')
                                    .then((json) => {
                                        res.status(404).json(json);
                                    })
                                    .catch((error) => {
                                        res.status(501).json(error);
                                    });
                            } else {
                                validateRoleAndType(resource, decoded, req.method)
                                    .then((r) => {
                                        next();
                                    })
                                    .catch((error) => {
                                        buildJSON(error, true, 'Validation')
                                            .then((r) => {
                                                res.status(501).json(r);
                                            })
                                            .catch((e) => {
                                                res.status(404).json(e);
                                            });
                                    });
                            }
                        }
                    });
                } else {
                    buildJSON(resource, false, 'Resource')
                        .then((json) => {
                            res.status(404).json(json);
                        })
                        .catch((error) => {
                            res.status(501).json(error);
                        });
                }
            })
            .catch((error) => {
                buildJSON(error, true, 'Resource')
                    .then((json) => {
                        res.status(404).json(json);
                    })
                    .catch((err) => {
                        res.status(501).json(err);
                    });
            });
    }
};

module.exports = accessControl;

