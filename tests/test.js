/* jshint esversion:6 */
let supertest = require("supertest"),
    should = require('should'),
    server = supertest.agent("http://localhost:3030");
// UNIT test begin
describe('#1 Sample test: return home page',() => {
    // #1 should return home page
    it('#1-1 Should return home page(get method)',(done) => {
        // calling home page api
        server
            .get('/')
            .expect(200)
            .end((err,res) => {
                if(err) {
                    throw err;
                }
                should(res).have.property('status', 200);
                done();
            });
    });
    // #2 should return 404 with status
    it('#1-2 Should return 404', (done) => {
        server
            .post('/')
            .expect(404)
            .end((err, res) => {
                if(err) throw err;
                should(res).have.property('status', 404);
                done();
            });
    });

});
describe('#2 Real test. Access to API without auth', () => {
    // #3 should return 501
    it('#2-1 Should be return error without all  - 501 (development version)', (done) => {
        server
            .get('/api/development')
            .expect(501)
            .end((err, res) => {
                if(err) {
                    throw err;
                } else {
                    should(res).have.property('status', 501);
                    done();
                }
            });
    });
    // #4 should return 404
    it('#2-2 Should be return error without token - 404 (development version)', (done) => {
        server
            .get('/api/development')
            .set('referrer', 'test-login')
            .end((err, res) => {
                if(err) throw err;
                should(res).have.property('status', 404);
                done();
            });
    });
});

describe('#3 Real Test. Loginizarion', () => {
    // #5 should reeturn 405
    it('#3-2 Should return answer with error (405)', (done) => {
        server
            .get('/api/development/login')
            .set('referrer', 'test-login')
            .end((err, res) => {
                if(err) throw err;
                should(res).have.property('status', 405);
                done();
            });
    });
    // #6 should return 200 and token
    it('#3-2 Should return answer and token', (done) => {
        server
            .post('/api/development/login')
            .set('referrer', 'test-login')
            .send({
                "phone": "89969464629",
                "password": "3pm9skn5"
            })
            .end((err, res) => {
                if(err) throw err;
                should(res).have.property('status', 200);
                should(res.body.meta).have.property('token');
                done();
            });
    });
    // #7 should return 404 error
    it('#3-3 Should return error 404 (request without send)', (done)=> {
        server
            .post('/api/development/login')
            .set('referrer', 'test-login')
            .end((err, res) => {
                if(err) throw err;
                should(res).have.property('status', 404);
                done();
            });
    });
    // #8 Return error when password incorrected (404)
    it('#3-4 Should return error 404 (not correct password)', (done) => {
        server
            .post('/api/development/login')
            .set('referrer', 'test-login')
            .send({
                "phone": "89969464629",
                "password": "3pm10skn5"
            })
            .end((err,res) => {
                if(err) throw err;
                should(res).have.property('status', 404);
                done();
            });
    });
    // #9 Return error when phone is incorrected (404)
    it('#3-5 Should return error 404 (ont correct phone/username)', (done) => {
        server
            .post('/api/development/login')
            .set('referrer', 'test-login')
            .send({
                "phone": "89919464629",
                "password": "3pm9sknt"
            })
            .end((err, res) => {
                if(err) throw err;
                should(res).have.property('status', 404);
                done();
            });
    });
    // #10 Return error with not full data (404)
    it('#3-6 Should return error 404 (not full data - only password)', (done) => {
        server
            .post('/api/development/logn')
            .set('referrer', 'test-login')
            .send({
                "password": "3pm9skn5"
            })
            .end((err, res) => {
                if(err) throw err;
                should(res).have.property('status', 404);
                done();
            });
    });
    // #11 Return error with not full data (404)
    it('#3-7 Should return error 404 (not full data - only phone)', (done) => {
        server
            .post('/api/development/login')
            .set('referrer', 'test-login')
            .send({
                "phone": "89969464629"
            })
            .end((err, res) => {
                if(err) throw err;
                should(res).have.property('status', 404);
                done();
            });
    });
 });

// UNIT test end
